import com.chaosalpha.Answer;
import com.chaosalpha.handlers.choreographer.ChoreographerSaveHandler;
import com.chaosalpha.model.Model;
import com.chaosalpha.payloads.NewPersonPayload;
import org.easymock.EasyMock;
import org.junit.Test;

import java.util.Collections;
import java.util.UUID;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;

/**
 * Created by John.Grzechowiak1 on 5/1/2017.
 */
public class ChoreographerSaveHandlerTest {


    public void anInvalidChoreographerReturnsBadRequest() {
        NewPersonPayload newPerson = new NewPersonPayload();
        newPerson.setFirstName(""); // this makes the person invalid
        newPerson.setLastName("Last");
        newPerson.setAvailableTimes(null);
        newPerson.setCommuter(true);
        newPerson.setPhoneNumber("(123) 456-7890");
        newPerson.setPronouns("He/His/Him");
        assertFalse(newPerson.isValid());

        Model model = EasyMock.createMock(Model.class);
        replay(model);

        ChoreographerSaveHandler handler = new ChoreographerSaveHandler(model);
        assertEquals(new Answer(400), handler.process(newPerson, Collections.emptyMap(), false));
        assertEquals(new Answer(400), handler.process(newPerson, Collections.emptyMap(), true));

        verify(model);
    }


    public void aChoreographerIsCorrectlyCreated() {
        NewPersonPayload newPerson = new NewPersonPayload();
        newPerson.setFirstName("First");
        newPerson.setLastName("Last");
        newPerson.setAvailableTimes(null);
        newPerson.setCommuter(true);
        newPerson.setPhoneNumber("(123) 456-7890");
        newPerson.setPronouns("He/His/Him");
        assertTrue(newPerson.isValid());

        Model model = EasyMock.createMock(Model.class);
        expect(model.createChoreographer(
                "First",
                "Last",
                "He/His/Him",
                "(123) 456-7890",
                true,
                null)
        ).andReturn(1L);
        replay(model);

        ChoreographerSaveHandler handler = new ChoreographerSaveHandler(model);
        assertEquals(new Answer(200, "728084e8-7c9a-4133-a9a7-f2bb491ef436"), handler.process(newPerson, Collections.emptyMap(), false));

        verify(model);
    }

}
