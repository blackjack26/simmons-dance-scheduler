import com.chaosalpha.model.Choreographer;
import com.chaosalpha.model.Model;
import org.easymock.EasyMock;
import org.junit.Test;

import java.util.UUID;

/**
 * Created by Jack on 5/1/2017.
 * Comment
 *
 * @author Jack
 */
public class ChoreographerPageHandlerTest {


    public void aNonEmptyListIsHandledCorrectlyInHtmlOutput() {
        Model model = EasyMock.createMock(Model.class);

        Choreographer chor = new Choreographer(
                1L,
                "test",
                "last",
                "he/his/him",
                "(123) 456-7890",
                true
        );
    }

    public void emptyListIsHandledCorrectlyInJsonOutput() {
        Model model = EasyMock.createMock(Model.class);

    }

}
