$(function(){
    $("#main-content").hide(0, function(){
        $(this).fadeIn(500);
    });
});

function getMoment(day, hour, min) {
    var m = new moment();
    m.year(2016);
    m.date(9 + day);
    m.month(9);
    m.hour(hour);
    m.minute(min);
    m.second(0);
    m.millisecond(0);
    return m;
}

function notification(type, title, desc) {
    return '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' +
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
        '<strong>' + title + '</strong> ' + desc +
    '</div>';
}