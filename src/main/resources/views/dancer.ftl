<#include "header.ftl">

<div id="main-content">
    <#if 0 < model.getAllDancers()?size>
        <div id="table-div">
            <table class="table table-responsive table-striped table-hover">
                <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Pronouns</th>
                    <th>Phone Number</th>
                    <th>Commuter</th>
                    <th>Availability</th>
                </tr>
                </thead>
                <tbody>
                <#list model.getAllDancers() as dancer>
                    <tr data-id="${dancer.getId()}">
                        <td>${dancer.getFirstName()}</td>
                        <td>${dancer.getLastName()}</td>
                        <td>${dancer.getPronouns()}</td>
                        <td>${dancer.getPhoneNumber()}</td>
                        <td>${dancer.commuterStr()}</td>
                        <td><a href="javascript:void(0)" data-id="${dancer.getId()}" onclick="showAvailability(this)">View</a></td>
                    </tr>
                </#list>
                </tbody>
            </table>
        </div>
    </#if>
    <@none_exist objectPlural="dancers" objList=model.getAllDancers() />

    <br>

    <div style="width:100%;text-align:center">
        <button class="btn btn-primary" id="addDancerBtn" onclick="openAddModal()">Add New Dancer</button>
    </div>
</div>

<!-- Add Dancer Modal -->
<div class="modal fade" id="addDancerModal" tabindex="-1" role="dialog" aria-labelledby="dancerModalLbl">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="closeAddModal()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="dancerModalLbl">Dancer Form</h4>
            </div>
            <div class="modal-body">
                <!-- Dancer Form -->
                <form id="new-dancer-form" class="collapse in" style="margin-top:10px; margin-bottom: 0px">
                    <input type="hidden" class="form-control" id="dancerId">
                    <div class="row">
                        <div class="form-group col-xs-12 col-md-6">
                            <label for="firstName">First Name*</label>
                            <input type="text" class="form-control" id="firstName" required>
                        </div>
                        <div class="form-group col-xs-12 col-md-6">
                            <label for="lastName">Last Name*</label>
                            <input type="text" class="form-control" id="lastName" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12 col-md-12">
                            <label for="pronouns">Preferred Pronouns (Comma Separated)</label>
                            <input type="text" class="form-control" id="pronouns">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12 col-md-6">
                            <label for="phone">Phone Number*</label>
                            <input type="text" class="form-control" id="phone" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12 col-md-6">
                            <div class="checkbox">
                                <label>
                                    <input id="commuter" type="checkbox" value="">
                                    <b>Commuter</b>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12 col-md-12">
                            <label for="available">Available Times*</label>
                            <div id="available"></div>
                        </div>
                    </div>
                </form>
                <!-- End Dancer Form -->
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="form-group col-xs-12 col-md-12" style="margin-bottom: 0px;">
                        <button type="button" class="btn btn-danger" id="delete" onclick="$('#deleteDancerModal').modal('show')">Delete <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                        <button type="button" class="btn btn-primary" id="add-new" onclick="addDancer()">Add Dancer</button>
                        <button type="button" class="btn btn-default" id="reset" onclick="clearForm()">Clear <span class="glyphicon glyphicon-repeat" aria-hidden="true"></span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Availability Modal -->
<div class="modal fade" id="viewAvailability" tabindex="-1" role="dialog" aria-labelledby="availabilityLbl">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="hideAvailability()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="availabilityLbl">Availability</h4>
            </div>
            <div class="modal-body">
                <div id="modalCalendar"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="hideAvailability()">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Delete Meeting Modal -->
<@delete_modal id="deleteMeetingModal" object="event" deleteFtn="deleteEvent()" />

<!-- Delete Dancer Modal -->
<@delete_modal id="deleteDancerModal" object="dancer" deleteFtn="deleteDancer()" />

<script>
    $( document ).ready(function(){

        $("#phone").mask("(000) 000-0000");

        $("#addDancerModal").on('hide.bs.modal', function() {
            clearForm();
        });
        $("#addDancerModal").on('shown.bs.modal', function() {
            $("#available").fullCalendar({
                header: {
                    left: '',
                    center: '',
                    right: ''
                },
                defaultView: 'agendaWeek',
                columnFormat: 'dddd',
                defaultDate: '2016-10-09',
                allDaySlot: false,
                scrollTime: '09:00:00',
                selectable: true,
                selectHelper: true,
                select: function(start, end) {
                    var id = $("#available").fullCalendar('clientEvents').length;
                    while ($("#available").fullCalendar('clientEvents', id).length > 0) {
                        id++;
                    }
                    eventData = {
                        title: "Available",
                        start: start,
                        end: end,
                        id: id
                    };
                    $("#available").fullCalendar('renderEvent', eventData, true);
                    $("#available").fullCalendar('unselect');
                },
                eventClick: function(calEvent, jsEvent, view) {
                    $("#deleteMeetingModal").modal("show");
                    $("#deleteMeetingModal").data("id", calEvent.id);
                },
                editable: true,
                longPressDelay: 1000
            });
        });

        $("#modalCalendar").fullCalendar({
            header: {
                left: '',
                center: '',
                right: ''
            },
            defaultView: 'agendaWeek',
            columnFormat: 'dddd',
            defaultDate: '2016-10-09',
            allDaySlot: false,
            scrollTime: '09:00:00'
        });
        $("#viewAvailability").on('shown.bs.modal', function() {
            $("#modalCalendar").fullCalendar('render');
        }).on('hidden.bs.modal', function() {
            hideAvailability();
        });
        $(".form-control").focusout(function(){
            updateValidity(this);
        });

        $("tbody tr").click(function(e) {
             if(e.target.tagName == "A")
                return;
             editDancer(this);
        });
    });

    function openAddModal() {
        $("#addDancerModal").modal('show');
    }

    function closeAddModal() {
        $("#addDancerModal").modal('hide');
        $("#deleteDancerModal").modal('hide');
        clearForm();
    }

    function editDancer(el) {
        var id = $(el).data("id");
        clearForm();
        $("#addDancerModal").on('shown.bs.modal.edit', function() {
            $.ajax({
                url: '/dancers/' + id,
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                    $("#firstName").val(data.firstName);
                    $("#lastName").val(data.lastName);
                    $("#pronouns").val(data.pronouns);
                    $("#phone").val(data.phoneNumber);
                    $("#commuter").prop("checked", data.commuter);
                    $("#dancerId").val(data.id);

                    var avail = data.availableTimes;
                    for (var i = 0; i < avail.length; i++) {
                        var loadedId = avail[i].slotId;
                        var start = avail[i].startTime;
                        var end = avail[i].endTime;
                        var startTime = getMoment(avail[i].dayOfWeek, start.hour, start.minute);
                        var endTime = getMoment(avail[i].dayOfWeek, end.hour, end.minute);

                        var eventData = {
                            id: $("#available").fullCalendar('clientEvents').length,
                            loadedId: loadedId,
                            start: startTime,
                            end: endTime,
                            title: "Available"
                        };
                        $("#available").fullCalendar('renderEvent', eventData, true);
                    }
                    $("#add-new").text("Update Dancer");
                    $("#delete").show();
                    $("#addDancerModal").unbind(".edit");
                },
                error: function(er) {

                }
            });
        });
        openAddModal();
    }

    function deleteDancer() {
        $.ajax({
            url: '/dancers/' + $("#dancerId").val(),
            type: 'DELETE',
            success: function() {
                closeAddModal();
                $('html, body').animate({
                    scrollTop: 0
                }, 500, function() {
                    location.reload();
                });
            },
            error: function() {
                closeAddModal();
                $('html, body').animate({
                    scrollTop: 0
                }, 500, function() {
                    location.reload();
                });
            }
        });
    }

    function deleteEvent() {
        $("#available").fullCalendar('removeEvents', $("#deleteMeetingModal").data("id"));
        $("#deleteMeetingModal").modal("hide");
    }

    function hideAvailability() {
        $("#modalCalendar").fullCalendar('removeEvents');
        $("#availabilityLbl").text("Availability");
        $("#viewAvailability").modal('hide');
    }

    function showAvailability(el) {
        $("#viewAvailability").on("shown.bs.modal", function(){
            $.ajax({
                url: '/dancers/' + $(el).closest("tr").data("id"),
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                    $("#availabilityLbl").text("Availability - " + data.firstName + " " + data.lastName);
                    var avail = data.availableTimes;
                    for (var i = 0; i < avail.length; i++) {
                        var loadedId = avail[i].slotId;
                        var start = avail[i].startTime;
                        var end = avail[i].endTime;
                        var startTime = getMoment(avail[i].dayOfWeek, start.hour, start.minute);
                        var endTime = getMoment(avail[i].dayOfWeek, end.hour, end.minute);

                        var eventData = {
                            id: $("#modalCalendar").fullCalendar('clientEvents').length,
                            loadedId: loadedId,
                            start: startTime,
                            end: endTime,
                            title: "Available"
                        };
                        $("#modalCalendar").fullCalendar('renderEvent', eventData, true);
                    }
                    $("#viewAvailability").off("shown.bs.modal");
                },
                error: function(er) {

                }
            });
        });
        $("#viewAvailability").modal('show');
    }

    function updateValidity(el) {
        if ($(el).is(":invalid")) {
            $(el).closest(".form-group").addClass("has-error");
        } else {
            $(el).closest(".form-group").removeClass("has-error");
        }
    }

    function clearForm() {
        $("#new-dancer-form .form-control").each(function() {
            $(this).val("");
        });
        $("#commuter").prop("checked", false);
        $("#available").fullCalendar('removeEvents');
        $("#add-new").text("Add Dancer");
        $("#delete").hide();
    }

    function addDancer() {
        $("#new-dancer-form .form-control").each(function() {
            updateValidity(this);
        });
        if ($("#new-dancer-form .has-error").length > 0) {
            $('html, body').animate({
                scrollTop: $("#new-dancer-form").closest(".well").offset().top - 15
            }, 500);
            return;
        }
        var dancer = {
            firstName: $("#firstName").val(),
            lastName: $("#lastName").val(),
            pronouns: $("#pronouns").val(),
            phoneNumber: $("#phone").val(),
            commuter: $("#commuter").prop("checked"),
            availableTimes: getTimeSlots('#available')
        };

        var postUrl = '/dancers/saveDancer';
        if ($("#dancerId").val() != "")
            postUrl = '/dancers/saveDancer/' + $("#dancerId").val();

        $.ajax({
            url: postUrl,
            type: 'POST',
            data: JSON.stringify(dancer),
            dataType: 'json',
            success: function(data) {
                closeAddModal();
                $('html, body').animate({
                    scrollTop: 0
                }, 500, function() {
                    location.reload();
                });
            },
            error: function(er) {
                clearForm();
                $('html, body').animate({
                    scrollTop: 0
                }, 500, function() {
                    location.reload();
                });
            }
        });
    }

    function getTimeSlots(calId) {
        var timeSlots = [];
        $($(calId).fullCalendar('clientEvents')).each(function(){
            var startTime = {
                hour: this.start.hour(),
                minute: this.start.minute()
            };
            var endTime = {
                hour: this.end.hour(),
                minute: this.end.minute()
            };
            var dayOfWeek = this.start.day();
            var timeSlot = {
                startTime: startTime,
                endTime: endTime,
                dayOfWeek: dayOfWeek
            };
            timeSlots.push(timeSlot);
        });
        return timeSlots;
    }
</script>

<#include "footer.ftl">