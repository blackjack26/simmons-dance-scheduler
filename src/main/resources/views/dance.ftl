<#include "header.ftl">

<link rel="stylesheet" type="text/css" href="/css/jquery-ui.css" />

<div id="main-content">
    <div id="dance-view">
        <#list model.getAllDances() as dance>
            <div class="dance-wrapper panel panel-<#if dance.getTimeSlot() == 'Not set'>default<#else>success</#if>" data-id="${dance.getId()}">
                <div class="panel-heading">
                    <div class="row">
                        <h1 class="dance-title col-xs-12 col-md-6">${dance.getName()}</h1>
                        <div class="col-xs-12 col-md-6 dance-title-details">
                            <p class="dance-chor"><b>Choreographer:</b> <span>${model.getChoreographer(dance.getChoreographerId()).get().getFullName()}</span></p>
                            <p class="dance-time"><b>Time:</b> <span>${dance.getTimeSlot()}</span></p>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="top-dancers-div col-xs-12 col-md-6">
                            <h3>Top Dancers</h3>
                            <#list model.getTopDancers(dance.getId()) as top>
                                <p class="dancer-item"><span class="badge">${top?counter}</span>&nbsp;&nbsp;${top.getFullName()}</p>
                            </#list>
                        </div>
                        <div class="alt-dancers-div col-xs-12 col-md-6">
                            <h3>Alternative Dancers</h3>
                            <#list model.getAlternativeDancers(dance.getId()) as alt>
                                <p class="dancer-item"><span class="badge">${alt?counter}</span>&nbsp;&nbsp;${alt.getFullName()}</p>
                            </#list>
                        </div>
                    </div>
                </div>
            </div>
        </#list>
    </div>
    <@none_exist objectPlural="dances" objList=model.getAllDances() />

    <br>

    <div style="width:100%;text-align:center">
        <button class="btn btn-primary" id="addDanceBtn" onclick="openAddModal()">Add New Dance</button>
    </div>
</div>

<!-- Add Dance Modal -->
<div class="modal fade" id="addDanceModal" tabindex="-1" role="dialog" aria-labelledby="danceModalLbl">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="closeAddModal()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="danceModalLbl">New Dance</h4>
            </div>
            <div class="modal-body">
                <!-- Dance Form -->
                <form id="new-dance-form" class="collapse in" style="margin-top:10px; margin-bottom: 0px">
                    <input type="hidden" class="form-control" id="danceId">
                    <div class="row">
                        <div class="form-group col-xs-12 col-md-12">
                            <label for="name">Name of Dance*</label>
                            <input type="text" class="form-control" id="name" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12 col-md-12">
                            <label for="choreographer">Choreographer*</label>
                            <input type="text" class="form-control" id="choreographer" name="choreographer" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12 col-md-6">
                            <label for="top-dancers">Top Dancers</label>
                            <input type="text" class="form-control" id="top-dancers" name="top-dancers">
                            <ul class="dancer-list" id="top-dancers-list"></ul>
                        </div>
                        <div class="form-group col-xs-12 col-md-6">
                            <label for="alt-dancers">Alternative Dancers</label>
                            <input type="text" class="form-control" id="alt-dancers" name="alt-dancers">
                            <ul class="dancer-list" id="alt-dancers-list"></ul>
                        </div>
                    </div>
                    <p style="text-align: right; margin-bottom: 0"><i>Right click to remove dancer</i></p>
                </form> <!-- End Dance Form -->
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="form-group col-xs-12 col-md-12" style="margin-bottom: 0px;">
                        <button type="button" class="btn btn-danger" id="delete" onclick="$('#deleteDanceModal').modal('show')">Delete <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                        <button type="button" class="btn btn-primary" id="add-new" onclick="addDance()">Add Dance</button>
                        <button type="button" class="btn btn-default" id="reset" onclick="clearForm()">Clear <span class="glyphicon glyphicon-repeat" aria-hidden="true"></span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- End Dance Modal -->

<!-- Delete Dance Modal -->
<@delete_modal id="deleteDanceModal" object="dance" deleteFtn="deleteDance()" />

<script src="/js/jquery-ui.min.js" type="text/javascript"></script>
<script>
    $(function(){

        $(".panel-heading").click(function(e) {
             editDance($(this).closest(".dance-wrapper"));
        });

        $("#addDanceModal").on('hide.bs.modal', function() {
            clearForm();
        });

        $(".form-control").focusout(function(){
            updateValidity(this);
        });

        createAutocomplete("/choreographers/name/auto/", "choreographer", false, function(el, item) {
            $("#choreographer").data("id", item.id);
            updateValidity(el);
        }, function(el, keyCode) {
            var DELETE = 46, BACKSPACE = 8, DASH = 189, SQUOTE = 222;
            if ((65 <= keyCode && keyCode <= 90) || keyCode === SQUOTE ||
                 keyCode === DASH || keyCode === BACKSPACE || keyCode === DELETE) {
                $("#choreographer").data("id", "");
                updateValidity(el);
            }
        });

        createAutocomplete("/dancers/name/auto/", "top-dancers", false, function(el, item) {

            var idExists = false;
            $(".dancer-item").each(function() {
                if ($(this).data("id") == item.id) {
                    idExists = true;
                    return;
                }
            });

            if (idExists) {
                $("#addDanceModal .modal-body").prepend(notification("warning", "Duplicate dancer!", item.value + " has already been added to the dance."));
                return;
            }

            $li = createDancerItem(item);
            $("#top-dancers-list").append($li);
            $("#top-dancers").val("");
        });

        createAutocomplete("/dancers/name/auto/", "alt-dancers", false, function(el, item) {

            var idExists = false;
            $(".dancer-item").each(function() {
                if ($(this).data("id") == item.id) {
                    idExists = true;
                    return;
                }
            });

            if (idExists) {
                $("#addDanceModal .modal-body").prepend(notification("warning", "Duplicate dancer!", item.value + " has already been added to the dance."));
                return;
            }

            $li = createDancerItem(item);
            $("#alt-dancers-list").append($li);
            $("#alt-dancers").val("");
        });

        $("#top-dancers-list").sortable({
            connectWith: ".dancer-list"
        });
        $("#alt-dancers-list").sortable({
            connectWith: ".dancer-list"
        });
    });

    function createDancerItem(item) {
        $li = $('<li class="ui-sortable-handle dancer-item" data-id="' + item.id + '"><span>&equiv;</span>&nbsp;&nbsp; ' +
                    item.value +
                '</li>');

        $li.contextmenu(function(ev) {
            ev.preventDefault();
            $(this).remove();
            return false;
        });

        return $li;
    }

    function openAddModal() {
        $("#addDanceModal").modal('show');
    }
    function closeAddModal() {
        $("#addDanceModal").modal('hide');
        $("#deleteDanceModal").modal('hide');
        clearForm();
    }

    function clearForm() {
        $("#new-dance-form .form-control").val("");
        $("#new-dance-form .form-group").removeClass("has-error");
        $("#add-new").text("Add Dance");
        $(".dancer-list").html("");
        $("#delete").hide();
    }

    function updateValidity(el) {
        if ($(el).is(":invalid")) {
            $(el).closest(".form-group").addClass("has-error");
        } else {
            $(el).closest(".form-group").removeClass("has-error");
        }

        // Make sure proper id is selected
        if ($(el).attr("id") == "choreographer") {
            if ($(el).data("id") == "" || $(el).data("id") == undefined) {
                $(el).closest(".form-group").addClass("has-error");
            } else {
                $(el).closest(".form-group").removeClass("has-error");
            }
        }
    }

    function addDance() {
        $("#new-dance-form .form-control").each(function() {
            updateValidity(this);
        });

        if ($("#new-dance-form .has-error").length > 0) {
            return;
        }

        var topDancers = [];
        $("#top-dancers-list .dancer-item").each(function(){
            topDancers.push($(this).data("id"));
        });

        var altDancers = [];
        $("#alt-dancers-list .dancer-item").each(function(){
            altDancers.push($(this).data("id"));
        });

        var dance = {
            name: $("#name").val(),
            choreographer: $("#choreographer").data("id"),
            topDancers: topDancers,
            alternativeDancers: altDancers
        };

        var postUrl = '/dances/saveDance';
        // Update dance
        if ($("#danceId").val() != "")
            postUrl += '/' + $("#danceId").val();

        $.ajax({
            url: postUrl,
            type: 'POST',
            data: JSON.stringify(dance),
            dataType: 'json',
            success: function(data) {
                closeAddModal();
                $('html, body').animate({
                    scrollTop: 0
                }, 500, function() {
                    location.reload();
                });
            },
            error: function(er) {

            }
        });
    }

    function editDance(el) {
        var id = $(el).data("id");
        clearForm();
        $("#addDanceModal").on('shown.bs.modal.edit', function() {
            $.ajax({
                url: '/dances/' + id,
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                    $("#name").val(data.name);
                    $("#choreographer").data("id", data.choreographerId);
                    $("#choreographer").val($(el).find(".dance-chor span").text());
                    $("#danceId").val(data.id);

                    // Top dancers
                    for(var i = 0; i < Object.keys(data.topDancersMap).length; i++) {
                        var id = Object.keys(data.topDancersMap)[i];
                        var item = {
                            id: id,
                            value: data.topDancersMap[id]
                        };
                        $("#top-dancers-list").append(createDancerItem(item));
                    }

                    // Alternative dancers
                    for(var i = 0; i < Object.keys(data.alternativeDancersMap).length; i++) {
                        var id = Object.keys(data.alternativeDancersMap)[i];
                        var item = {
                            id: id,
                            value: data.alternativeDancersMap[id]
                        };
                        $("#alt-dancers-list").append(createDancerItem(item));
                    }

                    $("#add-new").text("Update Dance");
                    $("#delete").show();
                    $("#addDanceModal").unbind(".edit");
                },
                error: function(er) {

                }
            });
        });
        openAddModal();
    }

    function deleteDance() {
        $.ajax({
            url: '/dances/' + $("#danceId").val(),
            type: 'DELETE',
            success: function() {
                closeAddModal();
                $('html, body').animate({
                    scrollTop: 0
                }, 500, function() {
                    location.reload();
                });
            },
            error: function() {
                closeAddModal();
                $('html, body').animate({
                    scrollTop: 0
                }, 500, function() {
                    location.reload();
                });
            }
        });
    }

    function split( val ) {
        return val.split(/,\s*/);
    }
    function extractLast( term ) {
        return split( term ).pop();
    }
    function createAutocomplete(searchURL, name, multiple, _selectCallback, _keyDownCallback) {
        $("input[name='" + name + "']").unbind("keydown.autoload");

        $("input[name='" + name + "']")
            .autocomplete({
                minLength: 0,
                source: function (request, response){
                    $.getJSON(searchURL + extractLast( request.term ), {}, response );
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function(event, ui) {
                    if(multiple) {
                        var terms = split(this.value);
                        // remove the current input
                        terms.pop();
                        // add the selected item
                        terms.push(ui.item.value);
                        // add placeholder to get the comma-and-space at the end
                        terms.push("");
                        this.value = terms.join(", ");
                    } else {
                        this.value = ui.item.value;
                    }

                    if (_selectCallback !== undefined){
                        _selectCallback(this, ui.item);
                    }

                    return false;
                }
            })
            .on("keydown.autoload", function(event){
                // don't navigate away from the field on tab when selecting an item
                if(event.keyCode === $.ui.keyCode.TAB &&
                    $(this).autocomplete("instance").menu.active ) {
                    event.preventDefault();
                }

                if (_keyDownCallback !== undefined) {
                    _keyDownCallback(this, event.keyCode);
                }
            });
        }
</script>

<#include "footer.ftl">