<#include "cards/none_exist.ftl">
<#include "cards/delete_modal.ftl">

<!DOCTYPE html>
<html>
<head>
    <title>SCDC Scheduler</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/images/shark.png">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/fullcalendar.min.css">
    <link rel="stylesheet" type="text/css" href="/css/main.css">

    <!-- JavaScript -->
    <script src="/js/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="/js/jquery.mask.js" type="text/javascript"></script>
    <script src="/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/js/moment.js" type="text/javascript"></script>
    <script src="/js/fullcalendar.min.js" type="text/javascript"></script>
    <script src="/js/main.js" type="text/javascript"></script>
</head>
<body>
    <div id="site-header">
        <h1 id="site-title"><img src="/images/shark.png"> SCDC Scheduler</h1>
    </div>

    <div id="navigation">
        <ul id="nav-list">
            <#if request.pathInfo() != "/dances" && request.pathInfo() != "/choreographers" && request.pathInfo() != "/dancers">
                <li class="nav-item selected"><a href="/">Home</a></li><#else>
                <li class="nav-item"><a href="/">Home</a></li>
            </#if>
            <#if request.pathInfo() == "/choreographers">
                <li class="nav-item selected"><a href="/choreographers">Choreographers</a></li><#else>
                <li class="nav-item"><a href="/choreographers">Choreographers</a></li>
            </#if>
            <#if request.pathInfo() == "/dances">
                <li class="nav-item selected"><a href="/dances">Dances</a></li><#else>
                <li class="nav-item"><a href="/dances">Dances</a></li>
            </#if>
            <#if request.pathInfo() == "/dancers">
                <li class="nav-item selected"><a href="/dancers">Dancers</a></li><#else>
                <li class="nav-item"><a href="/dancers">Dancers</a></li>
            </#if>
        </ul>
    </div>