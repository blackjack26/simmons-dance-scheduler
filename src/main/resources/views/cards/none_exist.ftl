<#macro none_exist objectPlural objList>
    <#if objList?size == 0>
        <div id="large-none">
            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
            <p id="none-warning">No ${objectPlural} have been added yet.</p>
        </div>
    </#if>
</#macro>