<#macro delete_modal id object deleteFtn>
    <div class="modal fade" id="${id}" tabindex="-1" role="dialog" aria-labelledby="${id}Lbl">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="${id}Lbl">Delete ${object?cap_first}?</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this ${object}?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger" onclick="${deleteFtn}">Delete</button>
                </div>
            </div>
        </div>
    </div>
</#macro>