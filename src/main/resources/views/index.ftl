<#include "header.ftl">

<div id="main-content">
    <div id="table-wrapper">
        <div id="calendar"></div>
    </div>
</div>

<script>
    $( document ).ready(function(){
        $("#calendar").fullCalendar({
            header: {
                left: '',
                center: '',
                right: ''
            },
            defaultView: 'agendaWeek',
            columnFormat: 'dddd',
            defaultDate: '2016-10-09',
            allDaySlot: false,
            scrollTime: '09:00:00'
        });
    });
</script>

<#include "footer.ftl">