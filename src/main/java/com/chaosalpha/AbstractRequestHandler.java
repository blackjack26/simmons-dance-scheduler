package com.chaosalpha;

import com.chaosalpha.model.Model;
import com.chaosalpha.payloads.EmptyPayload;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import spark.Request;
import spark.Response;
import spark.Route;

import java.io.IOException;
import java.util.Map;

/**
 * Created by Jack on 9/11/2016.
 * Comment
 *
 * @author Jack
 */
public abstract class AbstractRequestHandler<V extends Validable> implements RequestHandler<V>, Route {

    private Class<V> valueClass;
    protected Model model;
    protected Request request;
    protected Response response;

    private static final int HTTP_BAD_REQUEST = 400;

    public AbstractRequestHandler(Class<V> valueClass, Model model) {
        this.valueClass = valueClass;
        this.model = model;
    }

    private static boolean shouldReturnHtml(Request request) {
        String accept = request.headers("Accept");
        return accept != null && accept.contains("text/html");
    }

    public String dataToJson(Object data) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            return mapper.writeValueAsString(data);
        } catch (IOException e) {
            throw new RuntimeException("IOException from a StringWrite?", e);
        }
    }

    @Override
    public Answer process(V value, Map<String, String> urlParams, boolean shouldReturnHtml) {
        if (value != null && !value.isValid()) {
            return new Answer(HTTP_BAD_REQUEST);
        } else {
            return processImpl(value, urlParams, shouldReturnHtml);
        }
    }

    protected abstract Answer processImpl(V value, Map<String, String> urlParams, boolean shouldReturnHtml);

    @Override
    public Object handle(Request request, Response response) throws Exception {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            V value = null;
            if (valueClass != EmptyPayload.class) {
                value = objectMapper.readValue(request.body(), valueClass);
            }

            this.response = response;
            this.request = request;
            this.request.session(true);

            if (shouldReturnHtml(this.request)) {
                this.response.type("text/html");
            } else {
                this.response.type("application/json");
            }

            Map<String, String> urlParams = this.request.params();
            Answer answer = process(value, urlParams, shouldReturnHtml(this.request));
            this.response.status(answer.getCode());
            this.response.body(answer.getBody());
            return answer.getBody();
        } catch (JsonMappingException e) {
            e.printStackTrace();
            this.response.status(400);
            this.response.body(e.getMessage());
            return e.getMessage();
        }
    }

}
