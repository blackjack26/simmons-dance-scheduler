package com.chaosalpha.model;

import com.chaosalpha.SQLiteDB;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Created by Jack on 5/1/2017.
 * Implements the basic model and uses the SQLite framework
 * for querying.
 *
 * @author Jack Grzechowiak
 */
public class SQLiteModel implements Model {

    private static final Logger LOGGER = Logger.getLogger(SQLiteModel.class.getName());
    private SQLiteDB db = null;

    public SQLiteModel() {
        this("dance_scheduler.sqlite");
    }

    public SQLiteModel(String filename) {
        db = new SQLiteDB(filename);
        // Initialize tables
        createPersonTable();
        createPersonAvailableTable();
        createDanceTable();
        createTopDancesTable();
        createDancerDanceTable();
    }

    private String logId(Long id) {
        return " [id:" + id + "]";
    }

    /**
     * Creates the `person` table. This table holds all the information for
     * both dancers and choreographers.
     */
    private void createPersonTable() {
        db.createTable("CREATE TABLE IF NOT EXISTS person (\n" +
                " id INTEGER PRIMARY KEY AUTOINCREMENT, \n" +
                " firstName text NOT NULL, \n" +
                " lastName text NOT NULL, \n" +
                " pronouns text, \n" +
                " phone text, \n" +
                " commuter bool, \n" +
                " role text NOT NULL\n" +
                ");"
        );
    }

    /**
     * Creates the `person_available` table. This table contains all the times
     * a person is available during the week.
     */
    private void createPersonAvailableTable() {
        db.createTable("CREATE TABLE IF NOT EXISTS person_available (\n" +
                " id INTEGER PRIMARY KEY AUTOINCREMENT, \n" +
                " startTime moment NOT NULL, \n" +
                " endTime moment NOT NULL, \n" +
                " dayOfWeek integer NOT NULL, \n" +
                " personId INTEGER NOT NULL, \n" +
                " FOREIGN KEY(personId) REFERENCES person(id) ON DELETE CASCADE ON UPDATE CASCADE\n" +
                ");");
    }

    /**
     * Creates the `dance` table. The table holds all the information related
     * to the dances being held.
     */
    private void createDanceTable() {
        db.createTable("CREATE TABLE IF NOT EXISTS dance (\n" +
                " id INTEGER PRIMARY KEY AUTOINCREMENT, \n" +
                " name text NOT NULL, \n" +
                " startTime moment, \n" +
                " endTime moment, \n" +
                " dayOfWeek integer, \n" +
                " choreographerId uuid NOT NULL, \n" +
                " FOREIGN KEY(choreographerId) REFERENCES person(id) ON UPDATE CASCADE\n" +
                ");");
    }

    /**
     * Creates the `top_dances` table. This table contains all of the top dances
     * each dancer wishes to be in.
     */
    private void createTopDancesTable() {
        db.createTable("CREATE TABLE IF NOT EXISTS top_dances (\n" +
                " danceId INTEGER NOT NULL, \n" +
                " dancerId INTEGER NOT NULL, \n" +
                " rank INTEGER NOT NULL, \n" +
                " PRIMARY KEY(danceId, dancerId), \n" +
                " FOREIGN KEY(danceId) REFERENCES dance(id) ON DELETE CASCADE ON UPDATE CASCADE, \n" +
                " FOREIGN KEY(dancerId) REFERENCES person(id) ON DELETE CASCADE ON UPDATE CASCADE\n" +
                ");");
    }

    /**
     * Creates the `top_dancers` table. This table contains all of the top dancers
     * each choreographer wishes to have in their dance
     */
    private void createDancerDanceTable() {
        db.createTable("CREATE TABLE IF NOT EXISTS dancer_dance (\n" +
                " danceId INTEGER NOT NULL, \n" +
                " dancerId INTEGER NOT NULL, \n" +
                " rank INTEGER NOT NULL, \n" +
                " type TEXT NOT NULL, \n" + // TOP, ALT, ACT
                " PRIMARY KEY(danceId, dancerId), \n" +
                " FOREIGN KEY(danceId) REFERENCES dance(id) ON DELETE CASCADE ON UPDATE CASCADE, \n" +
                " FOREIGN KEY(dancerId) REFERENCES person(id) ON DELETE CASCADE ON UPDATE CASCADE\n" +
                ");");
    }

    /**
     * Inserts a new person into the `person` table
     *
     * @return the UUID of the inserted person
     */
    private Long createPerson(String firstName, String lastName, String pronouns, String phoneNumber, boolean commuter, String role) {
        String sql = "INSERT INTO person VALUES (NULL,?,?,?,?,?,?)";

        try (PreparedStatement pStmt = db.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            if (pStmt == null)
                return null;

            pStmt.setString(1, firstName);
            pStmt.setString(2, lastName);
            pStmt.setString(3, pronouns);
            pStmt.setString(4, phoneNumber);
            pStmt.setBoolean(5, commuter);
            pStmt.setString(6, role);
            db.insert(pStmt);

            ResultSet rs = pStmt.getGeneratedKeys();
            Long id = -1L;
            if (rs.next()) {
                id = rs.getLong(1);
            }

            LOGGER.log(Level.FINE, "%s", "Created person " + firstName + " " + lastName + logId(id));
            return id;
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            return null;
        }
    }

    /**
     * Associates available times with a person
     * @param id The person's ID
     * @param availableTimes The time slots that the person is available
     * @return Long of person. If null, an error occurred.
     */
    private Long addAvailability(Long id, TimeSlot[] availableTimes) {
        if (id == null)
            return null;

        for (TimeSlot ts : availableTimes) {
            String sql = "INSERT INTO person_available VALUES (NULL,?,?,?,?)";
            try (PreparedStatement pStmt = db.getConnection().prepareStatement(sql)) {
                // Statement could not be created, but choreographer has been created
                if (pStmt == null) {
                    LOGGER.warning("Could not associated time with person.");
                    return id;
                }
                pStmt.setString(1, ts.getStartTime().toString());
                pStmt.setString(2, ts.getEndTime().toString());
                pStmt.setInt(3, ts.getDayOfWeek());
                pStmt.setLong(4, id);
                db.insert(pStmt);

                LOGGER.fine(() -> "Created time slot [" + ts.toString() + "] for person" + logId(id));
            } catch (SQLException e) {
                LOGGER.warning("Could not associated time with person.");
                LOGGER.log(Level.WARNING, e.getMessage(), e);
                return id;
            }
        }

        return id;
    }

    /**
     * Updates a person based on the given information
     */
    private void updatePerson(Long id, String firstName, String lastName, String pronouns, String phoneNumber, boolean commuter, String role) {
        String sql = "UPDATE person SET firstName = ? , " +
                "lastName = ? , " +
                "pronouns = ? , " +
                "phone = ? , " +
                "commuter = ? , " +
                "role = ? " +
                "WHERE id = ?";
        try (PreparedStatement pStmt = db.getConnection().prepareStatement(sql)) {
            pStmt.setString(1, firstName);
            pStmt.setString(2, lastName);
            pStmt.setString(3, pronouns);
            pStmt.setString(4, phoneNumber);
            pStmt.setBoolean(5, commuter);
            pStmt.setString(6, role);
            pStmt.setLong(7, id);
            db.update(pStmt);

            LOGGER.log(Level.FINE, () -> "Updated person " + firstName + " " + lastName + logId(id));
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
        }
    }

    /**
     * Deletes a person based on their ID
     * @param id the person's ID
     * @return true if the delete was successful
     */
    private boolean deletePerson(Long id) {
        try (PreparedStatement pStmt = db.getConnection().prepareStatement("DELETE FROM person WHERE id = ?")) {
            pStmt.setLong(1, id);
            db.delete(pStmt);

            LOGGER.fine(() -> "Deleted person" + logId(id));
            return true;
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            return false;
        }
    }

    @Override
    public Long createChoreographer(String firstName, String lastName, String pronouns, String phoneNumber, boolean commuter) {
        return createPerson(firstName, lastName, pronouns, phoneNumber, commuter, "CHOREOGRAPHER");
    }

    @Override
    public Long createChoreographer(String firstName, String lastName, String pronouns, String phoneNumber, boolean commuter, TimeSlot[] availableTimes) {
        Long id = createChoreographer(firstName, lastName, pronouns, phoneNumber, commuter);
        return addAvailability(id, availableTimes);
    }

    @Override
    public Long createDance(String name, Long choreographer) {
        if (!getChoreographer(choreographer).isPresent()) {
            LOGGER.warning(() -> "A choreographer with the ID [" + choreographer + "] does not exist.");
            return null;
        }

        String sql = "INSERT INTO dance (name,choreographerId) VALUES (?,?)";
        try (PreparedStatement pStmt = db.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            pStmt.setString(1, name);
            pStmt.setString(2, choreographer.toString());
            db.insert(pStmt);

            ResultSet rs = pStmt.getGeneratedKeys();
            Long id = -1L;
            if (rs.next()) {
                id = rs.getLong(1);
            }

            LOGGER.log(Level.FINE, "%s", "Created dance " + name + logId(id));
            return id;
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            return null;
        }
    }

    @Override
    public void addTopDance(Long personId, Long danceId, int rank) {
        if (!getDancer(personId).isPresent()) {
            LOGGER.warning(() -> "A dancer with the ID [" + personId + "] does not exist.");
            return;
        } else if (!getDance(danceId).isPresent()) {
            LOGGER.warning(() -> "A dance with the ID [" + danceId + "] does not exist.");
            return;
        }

        String sql = "INSERT INTO top_dances (dancerId, danceId, rank) VALUES (?,?,?)";
        try (PreparedStatement pStmt = db.getConnection().prepareStatement(sql)) {
            pStmt.setLong(1, personId);
            pStmt.setLong(2, danceId);
            pStmt.setInt(3, rank);
            db.insert(pStmt);

            LOGGER.log(Level.FINE, "%s", "Added top dance [" + danceId + "] to dancer [" + personId + "]");
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
        }
    }

    @Override
    public void addTopDancer(Long danceId, Long personId, int rank) {
        if (!getDance(danceId).isPresent()) {
            LOGGER.warning(() -> "A dance with the ID [" + danceId + "] does not exists");
            return;
        } else if (!getDancer(personId).isPresent()) {
            LOGGER.warning(() -> "A dancer with the ID [" + personId + "] does not exists");
            return;
        }

        String sql = "INSERT INTO dancer_dance (danceId, dancerId, rank, type) VALUES (?,?,?,'TOP')";
        try (PreparedStatement pStmt = db.getConnection().prepareStatement(sql)) {
            pStmt.setLong(1, danceId);
            pStmt.setLong(2, personId);
            pStmt.setInt(3, rank);
            db.insert(pStmt);

            LOGGER.log(Level.FINE, "%s", "Added top dancer [" + personId + "] to dance [" + danceId + "]");
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
        }
    }

    @Override
    public void updateTopDancer(Long danceId, Long personId, int rank) {
        if (!getDance(danceId).isPresent()) {
            LOGGER.warning(() -> "A dance with the ID [" + danceId + "] does not exists");
            return;
        } else if (!getDancer(personId).isPresent()) {
            LOGGER.warning(() -> "A dancer with the ID [" + personId + "] does not exists");
            return;
        }

        String sql = "SELECT * FROM dancer_dance WHERE danceId = ? AND dancerId = ?";
        try (PreparedStatement pStmt = db.getConnection().prepareStatement(sql)) {
            pStmt.setLong(1, danceId);
            pStmt.setLong(2, personId);
            ResultSet rs = pStmt.executeQuery();

            // Doesn't exist
            if (!rs.isBeforeFirst()) {
                addTopDancer(danceId, personId, rank);
            } else {
                int r = rs.getInt("rank");
                String type = rs.getString("type");
                if (r == rank && "TOP".equals(type)) { // rank& type hasn't changed, do nothing
                    LOGGER.fine(() -> "Update not needed for dance [" + danceId + "] and dancer [" + personId + "]");
                    return;
                }

                String updateSql = "UPDATE dancer_dance SET rank = ?, type = 'TOP' WHERE dancerId = ? AND danceId = ?";
                PreparedStatement upStmt = db.getConnection().prepareStatement(updateSql);
                upStmt.setInt(1, rank);
                upStmt.setLong(2, personId);
                upStmt.setLong(3, danceId);
                db.update(upStmt);

                LOGGER.fine(() -> "Updated top dancer [" + personId + "] to dance [" + danceId + "] to rank " + rank);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
        }
    }

    @Override
    public void addAlternativeDancer(Long danceId, Long personId, int rank) {
        if (!getDance(danceId).isPresent()) {
            LOGGER.warning(() -> "A dance with the ID [" + danceId + "] does not exists");
            return;
        } else if (!getDancer(personId).isPresent()) {
            LOGGER.warning(() -> "A dancer with the ID [" + personId + "] does not exists");
            return;
        }

        String sql = "INSERT INTO dancer_dance (danceId, dancerId, rank, type) VALUES (?,?,?,'ALT')";
        try (PreparedStatement pStmt = db.getConnection().prepareStatement(sql)) {
            pStmt.setLong(1, danceId);
            pStmt.setLong(2, personId);
            pStmt.setInt(3, rank);
            db.insert(pStmt);

            LOGGER.log(Level.FINE, "%s", "Added top dancer [" + personId + "] to dance [" + danceId + "]");
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
        }
    }

    @Override
    public void updateAlternativeDancer(Long danceId, Long personId, int rank) {
        if (!getDance(danceId).isPresent()) {
            LOGGER.warning(() -> "A dance with the ID [" + danceId + "] does not exists");
            return;
        } else if (!getDancer(personId).isPresent()) {
            LOGGER.warning(() -> "A dancer with the ID [" + personId + "] does not exists");
            return;
        }

        String sql = "SELECT * FROM dancer_dance WHERE danceId = ? AND dancerId = ?";
        try (PreparedStatement pStmt = db.getConnection().prepareStatement(sql)) {
            pStmt.setLong(1, danceId);
            pStmt.setLong(2, personId);
            ResultSet rs = pStmt.executeQuery();

            // Doesn't exist
            if (!rs.isBeforeFirst()) {
                addAlternativeDancer(danceId, personId, rank);
            } else {
                int r = rs.getInt("rank");
                String type = rs.getString("type");
                if (r == rank && "ALT".equals(type)) { // rank and type hasn't changed, do nothing
                    LOGGER.fine(() -> "Update not needed for dance [" + danceId + "] and dancer [" + personId + "]");
                    return;
                }

                String updateSql = "UPDATE dancer_dance SET rank = ?, type = 'ALT' WHERE dancerId = ? AND danceId = ?";
                PreparedStatement upStmt = db.getConnection().prepareStatement(updateSql);
                upStmt.setInt(1, rank);
                upStmt.setLong(2, personId);
                upStmt.setLong(3, danceId);
                db.update(upStmt);

                LOGGER.fine(() -> "Updated alternative dancer [" + personId + "] to dance [" + danceId + "] to rank " + rank);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
        }
    }

    @Override
    public Long createDancer(String firstName, String lastName, String pronouns, String phoneNumber, boolean commuter) {
        return createPerson(firstName, lastName, pronouns, phoneNumber, commuter, "DANCER");
    }

    @Override
    public Long createDancer(String firstName, String lastName, String pronouns, String phoneNumber, boolean commuter, TimeSlot[] availableTimes) {
        Long id = createDancer(firstName, lastName, pronouns, phoneNumber, commuter);
        return addAvailability(id, availableTimes);
    }

    @Override
    public void updateChoreographer(Long id, String firstName, String lastName, String pronouns, String phoneNumber, boolean commuter, TimeSlot[] availableTimes) {
        if (!getChoreographer(id).isPresent()) {
            LOGGER.info("Choreographer does not exist, skipping update.");
            return;
        }
        updatePerson(id, firstName, lastName, pronouns, phoneNumber, commuter, "CHOREOGRAPHER");
        addAvailability(id, availableTimes);
    }

    @Override
    public void updateDancer(Long id, String firstName, String lastName, String pronouns, String phoneNumber, boolean commuter, TimeSlot[] availableTimes) {
        if(!getDancer(id).isPresent()) {
            LOGGER.info("Dancer does not exist, skipping update.");
            return;
        }
        updatePerson(id, firstName, lastName, pronouns, phoneNumber, commuter, "DANCER");
        addAvailability(id, availableTimes);
    }

    @Override
    public void updateDance(Long id, String name, Long choreographer) {
        if(!getDance(id).isPresent()) {
            LOGGER.info("Dance does not exist, skipping update.");
            return;
        }
        String sql = "UPDATE dance SET name = ? , " +
                "choreographerId = ? " +
                "WHERE id = ?";
        try (PreparedStatement pStmt = db.getConnection().prepareStatement(sql)) {
            pStmt.setString(1, name);
            pStmt.setString(2, choreographer.toString());
            pStmt.setLong(3, id);
            db.update(pStmt);

            LOGGER.fine(() -> "Updated dance " + name + logId(id));
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
        }
    }

    @Override
    public boolean deleteChoreographer(Long choreographerId) {
        if(!getChoreographer(choreographerId).isPresent()) {
            LOGGER.info("Choreographer does not exist, skipping delete.");
            return true;
        }
        return deletePerson(choreographerId);
    }

    @Override
    public boolean deleteDancer(Long dancerId) {
        if(!getDancer(dancerId).isPresent()) {
            LOGGER.info("Dancer does not exist, skipping delete.");
            return true;
        }
        return deletePerson(dancerId);
    }

    @Override
    public boolean deleteTopDancer(Long dancerId, Long danceId) {
        if (!getDancer(dancerId).isPresent()) {
            LOGGER.info("Dancer does not exist, skipping delete.");
            return true;
        } else if (!getDance(danceId).isPresent()) {
            LOGGER.info("Dance does not exist, skipping delete.");
            return true;
        }
        try (PreparedStatement pStmt = db.getConnection().prepareStatement("DELETE FROM dancer_dance WHERE danceId = ? AND dancerId = ? AND type = 'TOP'")) {
            pStmt.setLong(1, danceId);
            pStmt.setLong(2, dancerId);
            db.delete(pStmt);

            LOGGER.fine(() -> "Deleted top dancer [" + dancerId + "] from dance [" + danceId + "]");
            return true;
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            return false;
        }
    }

    @Override
    public boolean deleteAlternativeDancer(Long dancerId, Long danceId) {
        if (!getDancer(dancerId).isPresent()) {
            LOGGER.info("Dancer does not exist, skipping delete.");
            return true;
        } else if (!getDance(danceId).isPresent()) {
            LOGGER.info("Dance does not exist, skipping delete.");
            return true;
        }
        try (PreparedStatement pStmt = db.getConnection().prepareStatement("DELETE FROM dancer_dance WHERE danceId = ? AND dancerId = ? AND type = 'ALT'")) {
            pStmt.setLong(1, danceId);
            pStmt.setLong(2, dancerId);
            db.delete(pStmt);

            LOGGER.fine(() -> "Deleted alternative dancer [" + dancerId + "] from dance [" + danceId + "]");
            return true;
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            return false;
        }
    }

    @Override
    public boolean deleteDance(Long danceId) {
        if(!getDance(danceId).isPresent()) {
            LOGGER.info("Dance does not exist, skipping delete.");
            return true;
        }
        try (PreparedStatement pStmt = db.getConnection().prepareStatement("DELETE FROM dance WHERE id = ?")) {
            pStmt.setLong(1, danceId);
            db.delete(pStmt);

            LOGGER.fine(() -> "Deleted dance" + logId(danceId));
            return true;
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            return false;
        }
    }

    @Override
    public List<Dance> getAllDances() {
        String sql = "SELECT * FROM dance";
        try (Statement stmt  = db.getConnection().createStatement();
             ResultSet rs    = stmt.executeQuery(sql)) {
            List<Dance> dances = new ArrayList<>();
            while(rs.next()) {
                Dance dance = new Dance(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getLong("choreographerId")
                );
                String startTime = rs.getString("startTime");
                String endTime = rs.getString("endTime");
                int dayOfWeek = rs.getInt("dayOfWeek");
                if (startTime != null && endTime != null && dayOfWeek > 0) {
                    TimeSlot ts = new TimeSlot(
                            Moment.of(startTime),
                            Moment.of(endTime),
                            dayOfWeek
                    );
                    dance.setTimeSlot(ts);
                }
                dances.add(dance);
            }
            return dances;
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            return Collections.emptyList();
        }
    }

    private List<TimeSlot> getTimeSlotsFor(Long personId) {
        String sql = "SELECT * FROM person_available WHERE personId = '" + personId + "'";
        try (Statement stmt  = db.getConnection().createStatement();
             ResultSet rs    = stmt.executeQuery(sql)) {
            List<TimeSlot> timeSlots = new ArrayList<>();
            while(rs.next()) {
                TimeSlot ts = new TimeSlot(
                        Moment.of(rs.getString("startTime")),
                        Moment.of(rs.getString("endTime")),
                        rs.getInt("dayOfWeek")
                );
                timeSlots.add(ts);
            }
            return timeSlots;
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            return Collections.emptyList();
        }
    }

    @Override
    public List<Choreographer> getAllChoreographers() {
        String sql = "SELECT * FROM person WHERE role = 'CHOREOGRAPHER';";
        try (Statement stmt  = db.getConnection().createStatement();
             ResultSet rs    = stmt.executeQuery(sql)) {
            List<Choreographer> choreographers = new ArrayList<>();
            while(rs.next()) {
                Choreographer choreographer = new Choreographer(
                        rs.getLong("id"),
                        rs.getString("firstName"),
                        rs.getString("lastName"),
                        rs.getString("pronouns"),
                        rs.getString("phone"),
                        rs.getBoolean("commuter")
                );
                List<TimeSlot> timeSlots = getTimeSlotsFor(choreographer.getId());
                choreographer.addAvailableTimes(timeSlots.toArray(new TimeSlot[timeSlots.size()]));
                choreographers.add(choreographer);
            }
            return choreographers;
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            return Collections.emptyList();
        }
    }

    @Override
    public List<Dancer> getAllDancers() {
        String sql = "SELECT * FROM person WHERE role = 'DANCER';";
        try (Statement stmt  = db.getConnection().createStatement();
             ResultSet rs    = stmt.executeQuery(sql)) {
            List<Dancer> dancers = new ArrayList<>();
            while(rs.next()) {
                Dancer dancer = new Dancer(
                        rs.getLong("id"),
                        rs.getString("firstName"),
                        rs.getString("lastName"),
                        rs.getString("pronouns"),
                        rs.getString("phone"),
                        rs.getBoolean("commuter")
                );
                List<TimeSlot> timeSlots = getTimeSlotsFor(dancer.getId());
                dancer.addAvailableTimes(timeSlots.toArray(new TimeSlot[timeSlots.size()]));
                dancers.add(dancer);
            }
            return dancers;
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            return Collections.emptyList();
        }
    }

    @Override
    public Optional<Choreographer> getChoreographer(Long choreographerId) {
        String sql = "SELECT * FROM person WHERE role = 'CHOREOGRAPHER' AND id = '" + choreographerId + "';";
        try (Statement stmt  = db.getConnection().createStatement();
             ResultSet rs    = stmt.executeQuery(sql)) {
            if (!rs.isBeforeFirst())
                return Optional.empty();
            Choreographer choreographer = new Choreographer(
                    rs.getLong("id"),
                    rs.getString("firstName"),
                    rs.getString("lastName"),
                    rs.getString("pronouns"),
                    rs.getString("phone"),
                    rs.getBoolean("commuter")
            );
            List<TimeSlot> timeSlots = getTimeSlotsFor(choreographer.getId());
            choreographer.addAvailableTimes(timeSlots.toArray(new TimeSlot[timeSlots.size()]));
            return Optional.of(choreographer);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            return Optional.empty();
        }
    }

    @Override
    public Optional<Dancer> getDancer(Long dancerId) {
        String sql = "SELECT * FROM person WHERE role = 'DANCER' AND id = '" + dancerId + "'";
        try (Statement stmt  = db.getConnection().createStatement();
             ResultSet rs    = stmt.executeQuery(sql)) {
            if (!rs.isBeforeFirst())
                return Optional.empty();
            Dancer dancer = new Dancer(
                    rs.getLong("id"),
                    rs.getString("firstName"),
                    rs.getString("lastName"),
                    rs.getString("pronouns"),
                    rs.getString("phone"),
                    rs.getBoolean("commuter")
            );
            List<TimeSlot> timeSlots = getTimeSlotsFor(dancer.getId());
            dancer.addAvailableTimes(timeSlots.toArray(new TimeSlot[timeSlots.size()]));
            return Optional.of(dancer);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            return Optional.empty();
        }
    }

    @Override
    public Optional<Dance> getDance(Long danceId) {
        String sql = "SELECT * FROM dance WHERE id = '" + danceId + "'";
        try (Statement stmt  = db.getConnection().createStatement();
             ResultSet rs    = stmt.executeQuery(sql)) {
            if (!rs.isBeforeFirst())
                return Optional.empty();
            Dance dance = new Dance(
                    rs.getLong("id"),
                    rs.getString("name"),
                    rs.getLong("choreographerId")
            );
            String startTime = rs.getString("startTime");
            String endTime = rs.getString("endTime");
            int dayOfWeek = rs.getInt("dayOfWeek");
            if (startTime != null && endTime != null && dayOfWeek > 0) {
                TimeSlot ts = new TimeSlot(
                        Moment.of(startTime),
                        Moment.of(endTime),
                        dayOfWeek
                );
                dance.setTimeSlot(ts);
            }

            getTopDancers(dance.getId()).forEach(dancer -> dance.addTopDancer(dancer.getId(), dancer.getFullName()));
            getAlternativeDancers(dance.getId()).forEach(dancer -> dance.addAlternativeDancer(dancer.getId(), dancer.getFullName()));
            return Optional.of(dance);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            return Optional.empty();
        }
    }

    @Override
    public List<Dancer> getTopDancers(Long danceId) {
        String sql = "SELECT * FROM dancer_dance WHERE type = 'TOP' AND danceId = ? ORDER BY rank ASC";
        try (PreparedStatement pStmt = db.getConnection().prepareStatement(sql)) {
            pStmt.setLong(1, danceId);
            ResultSet rs = pStmt.executeQuery();
            if (!rs.isBeforeFirst())
                return Collections.emptyList();

            List<Dancer> dancers = new ArrayList<>();
            while(rs.next()) {
                getDancer(rs.getLong("dancerId")).ifPresent(dancers::add);
            }
            return dancers;
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            return Collections.emptyList();
        }
    }

    @Override
    public List<Dancer> getAlternativeDancers(Long danceId) {
        String sql = "SELECT * FROM dancer_dance WHERE type = 'ALT' AND danceId = ? ORDER BY rank ASC";
        try (PreparedStatement pStmt = db.getConnection().prepareStatement(sql)) {
            pStmt.setLong(1, danceId);
            ResultSet rs = pStmt.executeQuery();
            if (!rs.isBeforeFirst())
                return Collections.emptyList();

            List<Dancer> dancers = new ArrayList<>();
            while(rs.next()) {
                getDancer(rs.getLong("dancerId")).ifPresent(dancers::add);
            }
            return dancers;
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            return Collections.emptyList();
        }
    }

    @Override
    public List<Choreographer> getChoreographersByName(String name) {
        String sql = "SELECT * FROM person WHERE role = 'CHOREOGRAPHER' AND (firstName || ' ' || lastName) LIKE ?";
        try (PreparedStatement stmt = db.getConnection().prepareStatement(sql)) {
            stmt.setString(1, "%" + name + "%");
            ResultSet rs = stmt.executeQuery();

            if (!rs.isBeforeFirst())
                return Collections.emptyList();

            List<Choreographer> choreographers = new ArrayList<>();
            while(rs.next()) {
                Choreographer choreographer = new Choreographer(
                        rs.getLong("id"),
                        rs.getString("firstName"),
                        rs.getString("lastName"),
                        rs.getString("pronouns"),
                        rs.getString("phone"),
                        rs.getBoolean("commuter")
                );
                List<TimeSlot> timeSlots = getTimeSlotsFor(choreographer.getId());
                choreographer.addAvailableTimes(timeSlots.toArray(new TimeSlot[timeSlots.size()]));
                choreographers.add(choreographer);
            }
            return choreographers;
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            return Collections.emptyList();
        }
    }

    @Override
    public List<Dancer> getDancersByName(String name) {
        String sql = "SELECT * FROM person WHERE role = 'DANCER' AND (firstName || ' ' || lastName) LIKE ?";
        try (PreparedStatement stmt = db.getConnection().prepareStatement(sql)) {
            stmt.setString(1, "%" + name + "%");
            ResultSet rs = stmt.executeQuery();

            if (!rs.isBeforeFirst())
                return Collections.emptyList();

            List<Dancer> dancers = new ArrayList<>();
            while(rs.next()) {
                Dancer dancer = new Dancer(
                        rs.getLong("id"),
                        rs.getString("firstName"),
                        rs.getString("lastName"),
                        rs.getString("pronouns"),
                        rs.getString("phone"),
                        rs.getBoolean("commuter")
                );
                List<TimeSlot> timeSlots = getTimeSlotsFor(dancer.getId());
                dancer.addAvailableTimes(timeSlots.toArray(new TimeSlot[timeSlots.size()]));
                dancers.add(dancer);
            }
            return dancers;
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            return Collections.emptyList();
        }
    }

    @Override
    public List<Dance> getDancesByName(String name) {
        return null;
    }
}
