package com.chaosalpha.model;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Jack Grzechowiak on 11/7/2016.
 *
 * Provides a structure to hold the hour and minute of time.
 * Also provides simple functions to compare different moments.
 *
 * @author Jack Grzechowiak
 */
public class Moment {

    private int hour;
    private int minute;
    private static final Logger LOGGER = Logger.getLogger(Moment.class.getName());

    private Moment(int hour, int min) {
        this.hour = hour;
        this.minute = min;
    }

    /**
     * Creates a new {@link Moment} based on the given hour and minute.
     * @param hour The hour of time (0-23)
     * @param minute The minute of time (0-59)
     * @return A {@link Moment} object
     */
    public static Moment of(int hour, int minute) {
        return new Moment(hour, minute);
    }

    /**
     * Creates a new {@link Moment} based on the given time string.
     * @param time A string representation of the time in hh:mm format.
     * @return A {@link Moment} object
     */
    public static Moment of(String time) {
        if(time.indexOf(':') < 0) {
            LOGGER.warning(() -> "Not a proper time format. Please use hh:mm.");
            return null;
        }

        String hourStr = time.substring(0, time.indexOf(':'));
        String minStr = time.substring(time.indexOf(':') + 1);
        try {
            int hour = Integer.parseInt(hourStr);
            int min = Integer.parseInt(minStr);
            return new Moment(hour, min);
        } catch (NumberFormatException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            return null;
        }
    }

    /**
     * Determines if the current {@link Moment} comes before the inputted {@link Moment}.
     * @param m The time to check against
     * @return true if the current time is before the given time.
     */
    public boolean isBefore(Moment m) {
        return (this.hour < m.hour) || (this.hour == m.hour && this.minute < m.minute);
    }

    /**
     * Determines if the current {@link Moment} comes after the inputted {@link Moment}.
     * @param m The time to check against
     * @return true if the current time is after the given time.
     */
    public boolean isAfter(Moment m) {
        return (this.hour > m.hour) || (this.hour == m.hour && this.minute > m.minute);
    }
    
    public boolean isSameAs(Moment m) {
        return this.hour == m.hour && this.minute == m.minute;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    @Override
    public String toString() {
        return hour + ":" + minute;
    }
}
