package com.chaosalpha.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Jack on 9/10/2016.
 * Comment
 *
 * @author Jack
 */
public class Dancer extends Person{

    private Long dancerId;

    private List<Long> topDances;
    private int maxNumberOfDances = 1;

    public Dancer(){
    }

    @Override
    public Long getId() {
        return dancerId;
    }

    public Dancer(Long id, String firstName, String lastName, String pronouns, String phoneNumber) {
        this(id, firstName, lastName, pronouns, phoneNumber, false);
    }

    public Dancer(Long id, String firstName, String lastName, String pronouns, String phoneNumber, boolean commuter) {
        super(firstName, lastName, pronouns, phoneNumber, commuter);
        this.dancerId = id;
        this.topDances = new ArrayList<>();
    }

    public void addTopDance(Long danceId) {
        topDances.add(danceId);
    }
    public void addTopDances(List<Long> danceIds) {
        topDances.addAll(danceIds);
    }

}