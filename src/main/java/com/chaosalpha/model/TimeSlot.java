package com.chaosalpha.model;

import java.time.DayOfWeek;

/**
 * Created by Jack on 9/10/2016.
 * Comment
 *
 * @author Jack
 */
public class TimeSlot{
    private Moment startTime;
    private Moment endTime;
    private int dayOfWeek;

    public static final TimeSlot UNSET = new TimeSlot();

    private TimeSlot(){
    }

    public TimeSlot(int startHour, int startMin, int endHour, int endMin, int day) {
        this(Moment.of(startHour, startMin), Moment.of(endHour, endMin), day);
    }

    public TimeSlot(Moment start, Moment end, int day) {
        this.startTime = start;
        this.endTime = end;
        this.dayOfWeek = day;
    }

    public boolean overlaps(TimeSlot slot) {
        return dayOfWeek == slot.dayOfWeek
                && startTime.isBefore(slot.endTime)
                && endTime.isAfter(slot.startTime);
    }

    public boolean equals(TimeSlot slot) {
        return dayOfWeek == slot.dayOfWeek
                && startTime.isSameAs(slot.startTime)
                && endTime.isSameAs(slot.endTime);
    }

    public Moment getEndTime() {
        return endTime;
    }

    public Moment getStartTime() {
        return startTime;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    @Override
    public String toString() {
        if (this == UNSET)
            return "Not set";
        return DayOfWeek.of(dayOfWeek+1).toString() + " " + startTime.toString() + "-" + endTime.toString();
    }
}