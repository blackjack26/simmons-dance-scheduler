package com.chaosalpha.model;

/**
 * Created by Jack on 9/10/2016.
 * Comment
 *
 * @author Jack
 */
public class Choreographer extends Person{

    private Long choreographerId;

    private Choreographer() {
    }

    @Override
    public Long getId() {
        return choreographerId;
    }

    public Choreographer(Long id, String firstName, String lastName, String pronouns, String phoneNumber) {
        this(id, firstName, lastName, pronouns, phoneNumber, false);
    }

    public Choreographer(Long id, String firstName, String lastName, String pronouns, String phoneNumber, boolean commuter) {
        super(firstName, lastName, pronouns, phoneNumber, commuter);
        this.choreographerId = id;
    }
}