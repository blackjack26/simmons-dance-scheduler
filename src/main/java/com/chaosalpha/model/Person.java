package com.chaosalpha.model;

import com.chaosalpha.util.Updatable;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.sql.Time;
import java.util.*;

/**
 * Created by Jack on 9/11/2016.
 * Comment
 *
 * @author Jack
 */
public abstract class Person implements Updatable{

    private String firstName;
    private String lastName;
    private String pronouns = "";
    private String phoneNumber;
    private boolean commuter = false;

    private TimeSlot[] availableTimes;

    public Person() {
    }

    public Person(String firstName, String lastName, String pronouns, String phoneNumber) {
        this(firstName, lastName, pronouns, phoneNumber, false);
    }

    public Person(String firstName, String lastName, String pronouns, String phoneNumber, boolean commuter) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.pronouns = pronouns;
        this.phoneNumber = phoneNumber;
        this.commuter = commuter;

        availableTimes = new TimeSlot[0];
    }

    public void addAvailableTime(TimeSlot timeSlot) {
        TimeSlot[] temp = new TimeSlot[availableTimes.length + 1];
        System.arraycopy(availableTimes, 0, temp, 0, availableTimes.length);
        temp[temp.length - 1] = timeSlot;
        availableTimes = temp;
    }
    public void addAvailableTimes(TimeSlot[] timeSlots) {
        TimeSlot[] temp = new TimeSlot[availableTimes.length + timeSlots.length];
        System.arraycopy(availableTimes, 0, temp, 0, availableTimes.length);
        System.arraycopy(timeSlots, 0, temp, availableTimes.length, timeSlots.length);
        availableTimes = temp;
    }

    //region Getters
    @JsonIgnore
    public String getFullName() {
        return firstName + " " + lastName;
    }

    @JsonIgnore
    public String getAlphabeticalName() {
        return lastName + ", " + firstName;
    }

    public boolean isCommuter() {
        return commuter;
    }

    public String commuterStr() {
        return isCommuter() ? "Yes" : "No";
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public TimeSlot[] getAvailableTimes() {
        return availableTimes;
    }

    public String getPronouns() {
        return pronouns;
    }

    //endregion
}
