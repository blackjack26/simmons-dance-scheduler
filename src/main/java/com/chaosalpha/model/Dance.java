package com.chaosalpha.model;

import com.chaosalpha.util.Updatable;

import java.sql.Time;
import java.util.*;

/**
 * Created by Jack on 9/10/2016.
 * Comment
 *
 * @author Jack
 */
public class Dance implements Updatable{

    private Long danceId;
    private String name;
    private TimeSlot timeSlot;
    private Long choreographerId;

    private List<Long> dancers;
    private List<Long> topDancers;
    private List<Long> alternativeDancers;
    private Map<Long,String> topDancersMap;
    private Map<Long,String> alternativeDancersMap;

    private int minDancers = 8;

    public Dance(){
    }

    public Dance(Long id, String name, Long choreographerId) {
        this.danceId = id;
        this.name = name;
        this.choreographerId = choreographerId;
        dancers = new ArrayList<>();
        topDancers = new ArrayList<>();
        topDancersMap = new HashMap<>();
        alternativeDancers = new ArrayList<>();
        alternativeDancersMap = new HashMap<>();
        timeSlot = TimeSlot.UNSET;
    }

    public void addDancer(Long dancerId) {
        dancers.add(dancerId);
    }
    public void addDancers(List<Long> dancerIds) {
        dancers.addAll(dancerIds);
    }
    public List<Long> getDancers() {
        return dancers;
    }

    public void addTopDancer(Long dancerId) {
        topDancers.add(dancerId);
    }
    public void addTopDancer(Long id, String name) {
        topDancersMap.put(id, name);
    }
    public void addTopDancers(List<Long> dancerIds) {
        topDancers.addAll(dancerIds);
    }
    public List<Long> getTopDancers() {
        return topDancers;
    }
    public Map<Long, String> getTopDancersMap() {
        return topDancersMap;
    }

    public void addAlternativeDancer(Long dancerId) {
        alternativeDancers.add(dancerId);
    }
    public void addAlternativeDancer(Long id, String name) { alternativeDancersMap.put(id, name); }
    public void addAlternativeDancers(List<Long> dancerIds) {
        alternativeDancers.addAll(dancerIds);
    }
    public List<Long> getAlternativeDancers() {
        return alternativeDancers;
    }
    public Map<Long, String> getAlternativeDancersMap() {
        return alternativeDancersMap;
    }

    public void setTimeSlot(TimeSlot timeSlot) {
        this.timeSlot = timeSlot;
    }

    public String getName() {
        return name;
    }
    public Long getChoreographerId() {
        return choreographerId;
    }

    public TimeSlot getTimeSlot() {
        return timeSlot;
    }

    @Override
    public Long getId() {
        return danceId;
    }
}