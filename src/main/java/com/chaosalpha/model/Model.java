package com.chaosalpha.model;

import java.util.List;
import java.util.Optional;

/**
 * Created by Jack Grzechowiak on 9/11/2016.
 *
 * Basic model interface that provides the methods needed for
 * the system to function. This interface allows for easy model
 * switching without the system needing to know the difference.
 *
 * @author Jack Grzechowiak
 */
public interface Model {

    // Choreographer
    Long createChoreographer(String firstName, String lastName, String pronouns, String phoneNumber, boolean commuter);
    Long createChoreographer(String firstName, String lastName, String pronouns, String phoneNumber, boolean commuter, TimeSlot[] availableTimes);
    void updateChoreographer(Long id, String firstName, String lastName, String pronouns, String phoneNumber, boolean commuter, TimeSlot[] availableTimes);
    boolean deleteChoreographer(Long choreographerId);
    List<Choreographer> getAllChoreographers();
    Optional<Choreographer> getChoreographer(Long choreographerId);
    List<Choreographer> getChoreographersByName(String name);

    // Dancer
    Long createDancer(String firstName, String lastName, String pronouns, String phoneNumber, boolean commuter);
    Long createDancer(String firstName, String lastName, String pronouns, String phoneNumber, boolean commuter, TimeSlot[] availableTimes);
    void updateDancer(Long id, String firstName, String lastName, String pronouns, String phoneNumber, boolean commuter, TimeSlot[] availableTimes);
    boolean deleteDancer(Long dancerId);
    List<Dancer> getAllDancers();
    Optional<Dancer> getDancer(Long dancerId);
    List<Dancer> getDancersByName(String name);
    // Dancer Top Dances
    void addTopDance(Long personId, Long danceId, int rank);

    // Dance
    Long createDance(String name, Long choreographer);
    void updateDance(Long id, String name, Long choreographer);
    boolean deleteDance(Long danceId);
    List<Dance> getAllDances();
    Optional<Dance> getDance(Long danceId);
    List<Dance> getDancesByName(String name);
    // Dance Top Dancers
    void addTopDancer(Long danceId, Long personId, int rank);
    void updateTopDancer(Long danceId, Long personId, int rank);
    boolean deleteTopDancer(Long dancerId, Long danceId);
    List<Dancer> getTopDancers(Long danceId);
    // Dance Alternative Dancers
    void addAlternativeDancer(Long danceId, Long personId, int rank);
    void updateAlternativeDancer(Long danceId, Long personId, int rank);
    boolean deleteAlternativeDancer(Long dancerId, Long danceId);
    List<Dancer> getAlternativeDancers(Long danceId);

}
