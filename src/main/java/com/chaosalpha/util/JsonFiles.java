package com.chaosalpha.util;

/**
 * Created by Jack Grzechowiak on 9/28/2016.
 *
 * @author Jack Grzechowiak
 */
public class JsonFiles {

    private static String folder = "json";
    private static final String DANCER_FILE = "dancers.json";
    private static final String CHOREOGRAPHER_FILE = "choreographers.json";
    private static final String DANCE_FILE = "dances.json";
    private static final String TIMESLOT_FILE = "timeslots.json";

    public static String dancerFile(){
        return folder + "/" + DANCER_FILE;
    }

    public static String choreographerFile(){
        return folder + "/" + CHOREOGRAPHER_FILE;
    }

    public static String danceFile(){
        return folder + "/" + DANCE_FILE;
    }

    public static String timeSlotFile(){
        return folder + "/" + TIMESLOT_FILE;
    }

    public static void setFolder(String folder) {
        JsonFiles.folder = folder;
    }
}
