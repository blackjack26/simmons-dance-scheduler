package com.chaosalpha.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.InvalidPathException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Jack on 9/12/2016.
 * Comment
 *
 * @author Jack
 */
public class SaveManager {

    private static final Logger LOGGER = Logger.getLogger(SaveManager.class.getName());
    public static final String FILE_PREFIX = "./src/main/resources/";

    public static void removeFile(String path) {
        File file = new File(FILE_PREFIX + path);
        if (file.exists()) {
            file.delete();
        }
    }

    public static <T> void addToArray(String path, T object, Class<T[]> clazz) {
        ArrayList<T> current = new ArrayList<>(SaveManager.loadArray(path, clazz));
        current.add(object);
        SaveManager.save(path, current);
    }

    public static void save(String path, Object object) {
        ObjectMapper mapper = new ObjectMapper();
        File file = new File(FILE_PREFIX + path);

        try {
            if (!file.createNewFile()) {
                LOGGER.info("Overwriting file [" + path + "].");
            }
            mapper.writeValue(file, object);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }

        LOGGER.info("New data saved to file [" + path + "].");
    }

    public static <T> void update(String path, T object, Class<T[]> clazz) {
        if (object instanceof Updatable) {
            ArrayList<T> current = new ArrayList<>(SaveManager.loadArray(path, clazz));
            boolean updated = false;
            for (int i = 0; i < current.size(); i++) {
                if (((Updatable)current.get(i)).getId().equals(((Updatable)object).getId())) {
                    current.set(i, object);
                    updated = true;
                    break;
                }
            }
            if (!updated)
                LOGGER.info("Object not found in data. Nothing was updated.");
            else
                save(path, current);
        } else {
            LOGGER.warning("Unable to update object. Object needs to implement Updatable");
        }
    }

    public static <T> boolean delete(String path, UUID objectId, Class<T[]> clazz) {
        ArrayList<T> current = new ArrayList<>(SaveManager.loadArray(path, clazz));
        boolean deleted = false;
        for (int i = 0; i < current.size(); i++) {
            if (((Updatable)current.get(i)).getId().equals(objectId)) {
                current.remove(i);
                deleted = true;
                break;
            }
        }
        if (!deleted) {
            LOGGER.info("Object not found in data. Nothing was deleted.");
            return false;
        } else {
            save(path, current);
            return true;
        }
    }

    public static <T> List<T> loadArray(String path, Class<T[]> clazz) {
        ObjectMapper mapper = new ObjectMapper();
        File file = new File(FILE_PREFIX + path);

        try {
            List<T> list = Arrays.asList(mapper.readValue(file, clazz));
            LOGGER.info("Loaded data array from file [" + path + "].");
            return list;
        } catch(FileNotFoundException e) {
            LOGGER.warning("File in location [" + FILE_PREFIX + path + "] does not exist.");
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
        }
        return Collections.emptyList();
    }

    public static <T> T loadObject(String path, Class<T> clazz) {
        ObjectMapper mapper=  new ObjectMapper();
        File file = new File(FILE_PREFIX + path);

        try {
            T obj = mapper.readValue(file, clazz);
            LOGGER.info("Loaded data from file [" + path + "].");
            return obj;
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }

}
