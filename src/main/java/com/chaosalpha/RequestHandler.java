package com.chaosalpha;

import java.util.Map;

/**
 * Created by Jack on 9/11/2016.
 * Comment
 *
 * @author Jack
 */
public interface RequestHandler<V extends Validable> {

    Answer process(V value, Map<String, String> urlParams, boolean shouldReturnHtml);

}
