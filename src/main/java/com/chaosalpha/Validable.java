package com.chaosalpha;

/**
 * Created by Jack on 9/11/2016.
 * Comment
 *
 * @author Jack
 */
public interface Validable {
    boolean isValid();
}
