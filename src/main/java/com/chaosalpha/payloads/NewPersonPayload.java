package com.chaosalpha.payloads;

import com.chaosalpha.Validable;
import com.chaosalpha.model.TimeSlot;

import java.util.ArrayList;

/**
 * Created by Jack Grzechowiak on 9/28/2016.
 *
 * @author Jack Grzechowiak
 */
public class NewPersonPayload implements Validable {

    private Long id;
    private String firstName;
    private String lastName;
    private String pronouns;
    private String phoneNumber;
    private TimeSlot[] availableTimes;
    private boolean commuter = false;

    @Override
    public boolean isValid() {
        return firstName != null && !firstName.isEmpty() &&
                lastName != null && !lastName.isEmpty() &&
                phoneNumber != null && !phoneNumber.isEmpty();
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setCommuter(boolean commuter) {
        this.commuter = commuter;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setPronouns(String pronouns) {
        this.pronouns = pronouns;
    }

    public void setAvailableTimes(TimeSlot[] availableTimes) {
        this.availableTimes = availableTimes;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPronouns() {
        return pronouns;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public boolean isCommuter() {
        return commuter;
    }

    public TimeSlot[] getAvailableTimes() {
        return availableTimes;
    }

    public Long getId() {
        return id;
    }
}
