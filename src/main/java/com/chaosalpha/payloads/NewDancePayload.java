package com.chaosalpha.payloads;

import com.chaosalpha.Validable;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Jack Grzechowiak on 9/28/2016.
 *
 * @author Jack Grzechowiak
 */
public class NewDancePayload implements Validable {

    private String name;
    private Long choreographer;
    private ArrayList<Long> topDancers;
    private ArrayList<Long> alternativeDancers;
    private ArrayList<Long> dancers;

    @Override
    public boolean isValid() {
        return name != null && !name.isEmpty() &&
                choreographer >= 0;
    }

    public String getName() {
        return name;
    }

    public Long getChoreographer() {
        return choreographer;
    }

    public ArrayList<Long> getAlternativeDancers() {
        return alternativeDancers;
    }

    public ArrayList<Long> getTopDancers() {
        return topDancers;
    }

    public ArrayList<Long> getDancers() {
        return dancers;
    }
}
