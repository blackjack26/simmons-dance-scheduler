package com.chaosalpha;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Jack on 5/1/2017.
 * Comment
 *
 * @author Jack
 */
public class SQLiteDB {

    private static final Logger LOGGER = Logger.getLogger(SQLiteDB.class.getName());
    private Connection conn = null;

    private SQLiteDB() {}

    public SQLiteDB(String filename) {
        conn = connect(filename);
        try {
            Statement stmt = conn.createStatement();
            stmt.execute("PRAGMA foreign_keys = ON");
            stmt.close();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Connect to the test.db database
     *
     * @return the Connection object
     */
    private Connection connect(String filename) {
        String url = "jdbc:sqlite:db/" + filename;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
            LOGGER.info("Successfully connected to database.");
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return conn;
    }

    public void createTable(String sql) {
        try (Statement stmt = conn.createStatement()) {
            stmt.execute(sql);
            LOGGER.finer("Performed create query.");
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
        }
    }

    public void insert(PreparedStatement pStmt) {
        try {
            pStmt.executeUpdate();
            LOGGER.finer("Performed insert query.");
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
        }
    }

    public void update(PreparedStatement pStmt) {
        try {
            pStmt.executeUpdate();
            LOGGER.finer("Performed update query.");
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
        }
    }

    public void delete(PreparedStatement pStmt) {
        try {
            pStmt.executeUpdate();
            LOGGER.finer("Performed delete query.");
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
        }
    }

    public Connection getConnection() {
        return conn;
    }
}