package com.chaosalpha;

/**
 * Created by Jack on 9/11/2016.
 * Comment
 *
 * @author Jack
 */
public class Answer {

    private int code;
    private String body;

    public Answer(int code) {
        this.code = code;
        this.body = "";
    }

    public Answer(int code, String body) {
        this.code = code;
        this.body = body;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;

        Answer answer = (Answer)obj;

        if (code != answer.code)
            return false;
        if (body != null ? !body.equals(answer.body) : answer.body != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = code;
        result = 31 * result + (body != null ? body.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Answer(code=" + code + ", body=" + body + ")";
    }

    public String getBody() {
        return body;
    }

    public int getCode() {
        return code;
    }

    public static Answer ok(String body) {
        return new Answer(200, body);
    }
    public static Answer badRequest() {
        return new Answer(400);
    }
    public static Answer badRequest(String body) {
        return new Answer(400, body);
    }
}