package com.chaosalpha;

import com.chaosalpha.handlers.IndexHandler;
import com.chaosalpha.handlers.choreographer.ChoreographerPageHandler;
import com.chaosalpha.handlers.choreographer.ChoreographerSaveHandler;
import com.chaosalpha.handlers.choreographer.DeleteChoreographerHandler;
import com.chaosalpha.handlers.choreographer.GetChoreographerHandler;
import com.chaosalpha.handlers.dance.DancePageHandler;
import com.chaosalpha.handlers.dance.DanceSaveHandler;
import com.chaosalpha.handlers.dance.DeleteDanceHandler;
import com.chaosalpha.handlers.dance.GetDanceHandler;
import com.chaosalpha.handlers.dancer.DancerPageHandler;
import com.chaosalpha.handlers.dancer.DancerSaveHandler;
import com.chaosalpha.handlers.dancer.DeleteDancerHandler;
import com.chaosalpha.handlers.dancer.GetDancerHandler;
import com.chaosalpha.model.Model;
import com.chaosalpha.model.SQLiteModel;
import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import spark.template.freemarker.FreeMarkerEngine;

import java.util.logging.Logger;

import static spark.Spark.*;

/**
 * Created by Jack on 9/11/2016.
 * Comment
 *
 * @author Jack
 */
public class DanceSchedulerService {

    private static FreeMarkerEngine engine;

    private DanceSchedulerService(){
    }

    public static void main(String[] args) {
        exception(Exception.class, (e, req, res) -> e.printStackTrace()); // print all exceptions


        staticFiles.location("/public");

        Model model = new SQLiteModel();

        FreeMarkerEngine freeMarkerEngine = new FreeMarkerEngine();
        Configuration freeMarkerConfig = new Configuration();
        freeMarkerConfig.setTemplateLoader(new ClassTemplateLoader(DanceSchedulerService.class, "/views"));
        freeMarkerEngine.setConfiguration(freeMarkerConfig);
        DanceSchedulerService.engine = freeMarkerEngine;

        /* *** Paths *** */
        get("/home", new IndexHandler(model));

        // Choreographer Paths
        get("/choreographers", new ChoreographerPageHandler(model));
        post("/choreographers/saveChoreographer", new ChoreographerSaveHandler(model));
        post("/choreographers/saveChoreographer/:id", new ChoreographerSaveHandler(model));
        delete("/choreographers/:id", new DeleteChoreographerHandler(model));
        get("/choreographers/all", new GetChoreographerHandler(model));
        get("/choreographers/:id", new GetChoreographerHandler(model));
        get("/choreographers/name/:name", new GetChoreographerHandler(model));
        get("/choreographers/name/:auto/:name", new GetChoreographerHandler(model));

        // Dance Paths
        get("/dances", new DancePageHandler(model));
        post("/dances/saveDance", new DanceSaveHandler(model));
        post("/dances/saveDance/:id", new DanceSaveHandler(model));
        delete("/dances/:id", new DeleteDanceHandler(model));
        get("/dances/all", new GetDanceHandler(model));
        get("/dances/:id", new GetDanceHandler(model));
        get("/dances/name/:name", new GetDanceHandler(model));

        // Dancer Paths
        get("/dancers", new DancerPageHandler(model));
        post("/dancers/saveDancer", new DancerSaveHandler(model));
        post("/dancers/saveDancer/:id", new DancerSaveHandler(model));
        delete("/dancers/:id", new DeleteDancerHandler(model));
        get("/dancers/all", new GetDancerHandler(model));
        get("/dancers/:id", new GetDancerHandler(model));
        get("/dancers/name/:name", new GetDancerHandler(model));
        get("/dancers/name/:auto/:name", new GetDancerHandler(model));

        // Check paths
        get("/alive", (req, res) -> "ok");

        // Catch-all
        get("*", new IndexHandler(model));

        Logger.getLogger(DanceSchedulerService.class.getName()).info("Service started on http://localhost:4567");
    }

    public static FreeMarkerEngine getEngine() {
        return engine;
    }
}
