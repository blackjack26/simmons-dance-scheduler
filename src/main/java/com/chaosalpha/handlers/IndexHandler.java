package com.chaosalpha.handlers;

import com.chaosalpha.AbstractRequestHandler;
import com.chaosalpha.Answer;
import com.chaosalpha.DanceSchedulerService;
import com.chaosalpha.model.Model;
import com.chaosalpha.payloads.EmptyPayload;
import spark.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jack on 9/14/2016.
 * Comment
 *
 * @author Jack
 */
public class IndexHandler extends AbstractRequestHandler<EmptyPayload> {

    public IndexHandler(Model model) {
        super(EmptyPayload.class, model);
    }

    @Override
    protected Answer processImpl(EmptyPayload value, Map<String, String> urlParams, boolean shouldReturnHtml) {
        if (shouldReturnHtml) {
            Map<String, Object> params = new HashMap<>();
            params.put("request", request);
            params.put("response", response);

            return Answer.ok(DanceSchedulerService.getEngine().render(new ModelAndView(params, "index.ftl")));
        } else {
            return Answer.ok(dataToJson(model.getAllDances()));
        }
    }
}
