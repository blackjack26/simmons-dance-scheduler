package com.chaosalpha.handlers.dancer;

import com.chaosalpha.AbstractRequestHandler;
import com.chaosalpha.Answer;
import com.chaosalpha.model.Dancer;
import com.chaosalpha.model.Model;
import com.chaosalpha.model.TimeSlot;
import com.chaosalpha.payloads.NewPersonPayload;

import java.sql.Time;
import java.util.*;

/**
 * Created by Jack Grzechowiak on 9/28/2016.
 *
 * @author Jack Grzechowiak
 */
public class DancerSaveHandler extends AbstractRequestHandler<NewPersonPayload> {
    public DancerSaveHandler(Model model) {
        super(NewPersonPayload.class, model);
    }

    @Override
    protected Answer processImpl(NewPersonPayload value, Map<String, String> urlParams, boolean shouldReturnHtml) {
        Long id;
        if (urlParams.get(":id") == null) {
            id = model.createDancer(value.getFirstName(), value.getLastName(), value.getPronouns(), value.getPhoneNumber(), value.isCommuter(), value.getAvailableTimes());
        } else {
            id = Long.valueOf(urlParams.get(":id"));

            // Do not save duplicate availabilities
            TimeSlot[] timeSlots = value.getAvailableTimes();
            Optional<Dancer> dancer = model.getDancer(id);
            if (dancer.isPresent()) {
                Dancer d = dancer.get();
                ArrayList<TimeSlot> oldSlots = new ArrayList<>(Arrays.asList(d.getAvailableTimes()));
                ArrayList<TimeSlot> uniqueSlots = new ArrayList<>(Arrays.asList(timeSlots));
                timeSlots = uniqueSlots.stream()
                        .filter(timeSlot -> oldSlots.stream()
                                .noneMatch(oldTimeSlot -> oldTimeSlot.overlaps(timeSlot)))
                        .toArray(TimeSlot[]::new);
            }

            model.updateDancer(id, value.getFirstName(), value.getLastName(), value.getPronouns(), value.getPhoneNumber(), value.isCommuter(), timeSlots);
        }
        return Answer.ok(id.toString());
    }
}
