package com.chaosalpha.handlers.dancer;

import com.chaosalpha.AbstractRequestHandler;
import com.chaosalpha.Answer;
import com.chaosalpha.model.Dancer;
import com.chaosalpha.model.Model;
import com.chaosalpha.payloads.EmptyPayload;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.chaosalpha.Answer.badRequest;
import static com.chaosalpha.Answer.ok;

/**
 * Created by Jack on 11/9/2016.
 * Comment
 *
 * @author Jack
 */
public class GetDancerHandler extends AbstractRequestHandler<EmptyPayload> {
    public GetDancerHandler(Model model) {
        super(EmptyPayload.class, model);
    }

    @Override
    protected Answer processImpl(EmptyPayload value, Map<String, String> urlParams, boolean shouldReturnHtml) {

        // Get Dancer By ID
        if (urlParams.get(":id") != null) {
            if (StringUtils.isNumeric(urlParams.get(":id"))) {
                Optional<Dancer> dancer = model.getDancer(Long.valueOf(urlParams.get(":id")));
                return dancer.map(dcr -> ok(dataToJson(dcr))).orElseGet(() -> ok("No data found"));
            }
            return badRequest("Invalid ID value");
        }
        // Get Dancers By Name
        else if (urlParams.get(":name") != null) {
            List<Dancer> dancers = model.getDancersByName(urlParams.get(":name"));

            // Formats the dancers for autocomplete searching
            if (urlParams.get(":auto") != null) {
                List<Map<String,String>> dancerMap = dancers.stream().map(dancer -> {
                    Map<String,String> map = new HashMap<>();
                    map.put("value", dancer.getFullName());
                    map.put("label", dancer.getFullName());
                    map.put("id", dancer.getId().toString());
                    return map;
                }).collect(Collectors.toList());
                return ok(dataToJson(dancerMap));
            }
            return ok(dataToJson(dancers));
        }
        // Get All Dancers
        else if (urlParams.isEmpty()) {
            List<Dancer> dancers = model.getAllDancers();
            return ok(dataToJson(dancers));
        }

        return badRequest("Invalid URL Parameters");
    }
}
