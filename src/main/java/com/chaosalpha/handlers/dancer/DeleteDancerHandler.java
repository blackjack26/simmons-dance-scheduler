package com.chaosalpha.handlers.dancer;

import com.chaosalpha.AbstractRequestHandler;
import com.chaosalpha.Answer;
import com.chaosalpha.model.Model;
import com.chaosalpha.payloads.EmptyPayload;

import java.util.Map;
import java.util.UUID;

/**
 * Created by Jack on 11/9/2016.
 * Comment
 *
 * @author Jack
 */
public class DeleteDancerHandler extends AbstractRequestHandler<EmptyPayload> {
    public DeleteDancerHandler(Model model) {
        super(EmptyPayload.class, model);
    }

    @Override
    protected Answer processImpl(EmptyPayload value, Map<String, String> urlParams, boolean shouldReturnHtml) {
        Long id = Long.valueOf(urlParams.get(":id"));
        return Answer.ok(Boolean.toString(model.deleteDancer(id)));
    }
}
