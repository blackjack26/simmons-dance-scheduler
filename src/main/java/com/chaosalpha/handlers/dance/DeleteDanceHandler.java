package com.chaosalpha.handlers.dance;

import com.chaosalpha.AbstractRequestHandler;
import com.chaosalpha.Answer;
import com.chaosalpha.model.Model;
import com.chaosalpha.payloads.EmptyPayload;

import java.util.Map;

/**
 * Created by Jack on 5/6/2017.
 * Comment
 *
 * @author Jack
 */
public class DeleteDanceHandler extends AbstractRequestHandler<EmptyPayload> {
    public DeleteDanceHandler(Model model) {
        super(EmptyPayload.class, model);
    }

    @Override
    protected Answer processImpl(EmptyPayload value, Map<String, String> urlParams, boolean shouldReturnHtml) {
        Long id = Long.valueOf(urlParams.get(":id"));
        return Answer.ok(Boolean.toString(model.deleteDance(id)));
    }
}
