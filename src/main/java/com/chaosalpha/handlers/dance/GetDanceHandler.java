package com.chaosalpha.handlers.dance;

import com.chaosalpha.AbstractRequestHandler;
import com.chaosalpha.Answer;
import com.chaosalpha.handlers.dancer.GetDancerHandler;
import com.chaosalpha.model.Dance;
import com.chaosalpha.model.Model;
import com.chaosalpha.payloads.EmptyPayload;
import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.chaosalpha.Answer.badRequest;
import static com.chaosalpha.Answer.ok;

/**
 * Created by Jack on 5/6/2017.
 * Comment
 *
 * @author Jack
 */
public class GetDanceHandler extends AbstractRequestHandler<EmptyPayload> {
    public GetDanceHandler(Model model) {
        super(EmptyPayload.class, model);
    }

    @Override
    protected Answer processImpl(EmptyPayload value, Map<String, String> urlParams, boolean shouldReturnHtml) {

        // Get Dance By ID
        if (urlParams.get(":id") != null) {
            if (StringUtils.isNumeric(urlParams.get(":id"))) {
                Optional<Dance> dance = model.getDance(Long.valueOf(urlParams.get(":id")));
                return dance.map(dance1 -> ok(dataToJson(dance1))).orElseGet(() -> ok("No data found"));
            }
            return badRequest("Invalid ID value");
        }
        // Get Dance By Name
        else if (urlParams.get(":name") != null) {
            List<Dance> dances = model.getDancesByName(urlParams.get(":name"));
            return ok(dataToJson(dances));
        }
        // Get All Dances
        else if (urlParams.isEmpty()) {
            List<Dance> dances = model.getAllDances();
            return ok(dataToJson(dances));
        }

        return badRequest("Invalid URL Parameters");
    }
}
