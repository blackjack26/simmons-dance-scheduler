package com.chaosalpha.handlers.dance;

import com.chaosalpha.AbstractRequestHandler;
import com.chaosalpha.Answer;
import com.chaosalpha.model.Dance;
import com.chaosalpha.model.Model;
import com.chaosalpha.payloads.NewDancePayload;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Jack Grzechowiak on 9/28/2016.
 *
 * @author Jack Grzechowiak
 */
public class DanceSaveHandler extends AbstractRequestHandler<NewDancePayload> {

    public DanceSaveHandler(Model model) {
        super(NewDancePayload.class, model);
    }

    @Override
    protected Answer processImpl(NewDancePayload value, Map<String, String> urlParams, boolean shouldReturnHtml) {
        Long id;
        if(urlParams.get(":id") == null) {
            id = model.createDance(value.getName(), value.getChoreographer());

            // Add Top Dancers
            int rank = 1;
            for(Long dId : value.getTopDancers()) {
                model.addTopDancer(id, dId, rank++);
            }

            // Add Alternative Dancers
            rank = 1;
            for (Long dId : value.getAlternativeDancers()) {
                model.addAlternativeDancer(id, dId, rank++);
            }
        } else {
            id = Long.valueOf(urlParams.get(":id"));
            Optional<Dance> opDance = model.getDance(id);
            if (!opDance.isPresent())
                return Answer.badRequest();

            model.updateDance(id, value.getName(), value.getChoreographer());
            Dance dance = opDance.get();

            // Remove absent dancers
            List<Long> removedTopDancers = new ArrayList<>();
            dance.getTopDancersMap().forEach((aLong, name) -> {
                if (!value.getTopDancers().contains(aLong))
                    removedTopDancers.add(aLong);
            });
            for (Long dId : removedTopDancers) {
                model.deleteTopDancer(dId, id);
            }
            List<Long> removedAltDancers = new ArrayList<>();
            dance.getAlternativeDancersMap().forEach((aLong, name) -> {
                if (!value.getAlternativeDancers().contains(aLong))
                    removedAltDancers.add(aLong);
            });
            for (Long dId : removedAltDancers) {
                model.deleteAlternativeDancer(dId, id);
            }

            // Update Top Dancers
            int rank = 1;
            for(Long dId : value.getTopDancers()) {
                model.updateTopDancer(id, dId, rank++);
            }

            // Update Alternative Dancers
            rank = 1;
            for (Long dId : value.getAlternativeDancers()) {
                model.updateAlternativeDancer(id, dId, rank++);
            }
        }
        return Answer.ok(id.toString());
    }
}
