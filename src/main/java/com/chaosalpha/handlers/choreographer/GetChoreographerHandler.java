package com.chaosalpha.handlers.choreographer;

import com.chaosalpha.AbstractRequestHandler;
import com.chaosalpha.Answer;
import com.chaosalpha.model.Choreographer;
import com.chaosalpha.model.Model;
import com.chaosalpha.payloads.EmptyPayload;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.chaosalpha.Answer.badRequest;
import static com.chaosalpha.Answer.ok;

/**
 * Created by Jack on 11/8/2016.
 * Comment
 *
 * @author Jack
 */
public class GetChoreographerHandler  extends AbstractRequestHandler<EmptyPayload> {
    public GetChoreographerHandler(Model model) {
        super(EmptyPayload.class, model);
    }

    @Override
    protected Answer processImpl(EmptyPayload value, Map<String, String> urlParams, boolean shouldReturnHtml) {

        // Get Choreographer By ID
        if (urlParams.get(":id") != null) {
            if (StringUtils.isNumeric(urlParams.get(":id"))) {
                Optional<Choreographer> chor = model.getChoreographer(Long.valueOf(urlParams.get(":id")));
                return chor.map(choreographer -> ok(dataToJson(choreographer))).orElseGet(() -> ok("No data found"));
            }
            return badRequest("Invalid ID value");
        }
        // Get Choreographer By Name
        else if (urlParams.get(":name") != null) {
            List<Choreographer> choreographers = model.getChoreographersByName(urlParams.get(":name"));

            // Formats the choreographers for autocomplete searching
            if (urlParams.get(":auto") != null) {
                List<Map<String,String>> chorMap = choreographers.stream().map(choreographer -> {
                    Map<String,String> map = new HashMap<>();
                    map.put("value",choreographer.getFullName());
                    map.put("label",choreographer.getFullName());
                    map.put("id",choreographer.getId().toString());
                    return map;
                }).collect(Collectors.toList());
                return ok(dataToJson(chorMap));
            }
            return ok(dataToJson(choreographers));
        }
        // Get All Choreographers
        else if (urlParams.isEmpty()) {
            List<Choreographer> choreographers = model.getAllChoreographers();
            return ok(dataToJson(choreographers));
        }

        return badRequest("Invalid URL Parameters");
    }
}
