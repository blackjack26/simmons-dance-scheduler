package com.chaosalpha.handlers.choreographer;

import com.chaosalpha.AbstractRequestHandler;
import com.chaosalpha.Answer;
import com.chaosalpha.DanceSchedulerService;
import com.chaosalpha.model.Model;
import com.chaosalpha.payloads.EmptyPayload;
import spark.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jack Grzechowiak on 9/28/2016.
 *
 * @author Jack Grzechowiak
 */
public class ChoreographerPageHandler extends AbstractRequestHandler<EmptyPayload> {

    public ChoreographerPageHandler(Model model) {
        super(EmptyPayload.class, model);
    }

    @Override
    protected Answer processImpl(EmptyPayload value, Map<String, String> urlParams, boolean shouldReturnHtml) {
        if (shouldReturnHtml) {
            Map<String, Object> params = new HashMap<>();
            params.put("request", request);
            params.put("response", response);
            params.put("model", model);

            return Answer.ok(DanceSchedulerService.getEngine().render(new ModelAndView(params, "choreographer.ftl")));
        } else {
            return Answer.ok(dataToJson(model.getAllChoreographers()));
        }
    }
}
