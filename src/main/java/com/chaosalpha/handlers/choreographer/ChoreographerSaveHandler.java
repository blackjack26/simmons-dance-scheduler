package com.chaosalpha.handlers.choreographer;

import com.chaosalpha.AbstractRequestHandler;
import com.chaosalpha.Answer;
import com.chaosalpha.model.Choreographer;
import com.chaosalpha.model.Model;
import com.chaosalpha.model.TimeSlot;
import com.chaosalpha.payloads.NewPersonPayload;

import java.util.*;

/**
 * Created by Jack Grzechowiak on 9/28/2016.
 *
 * @author Jack Grzechowiak
 */
public class ChoreographerSaveHandler extends AbstractRequestHandler<NewPersonPayload>{
    public ChoreographerSaveHandler(Model model) {
        super(NewPersonPayload.class, model);
    }

    @Override
    protected Answer processImpl(NewPersonPayload value, Map<String, String> urlParams, boolean shouldReturnHtml) {
        Long id;
        if (urlParams.get(":id") == null) {
            id = model.createChoreographer(value.getFirstName(), value.getLastName(), value.getPronouns(), value.getPhoneNumber(), value.isCommuter(), value.getAvailableTimes());
        } else {
            id = Long.valueOf(urlParams.get(":id"));

            // Do not save duplicate availabilities
            TimeSlot[] timeSlots = value.getAvailableTimes();
            Optional<Choreographer> choreographer = model.getChoreographer(id);
            if (choreographer.isPresent()) {
                Choreographer c = choreographer.get();
                ArrayList<TimeSlot> oldSlots = new ArrayList<>(Arrays.asList(c.getAvailableTimes()));
                ArrayList<TimeSlot> uniqueSlots = new ArrayList<>(Arrays.asList(timeSlots));
                timeSlots = uniqueSlots.stream()
                        .filter(timeSlot -> oldSlots.stream()
                                .noneMatch(oldTimeSlot -> oldTimeSlot.overlaps(timeSlot)))
                        .toArray(TimeSlot[]::new);
            }

            model.updateChoreographer(id, value.getFirstName(), value.getLastName(), value.getPronouns(), value.getPhoneNumber(), value.isCommuter(), timeSlots);
        }
        return Answer.ok(id.toString());
    }
}
