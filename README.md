# Simmons Dance Scheduler

## Synopsis

This application is a timetable scheduler for the Simmons College Dance Company to assist them in scheduling dances for the semester. The administrators can add dancers, choreographers, and dancers with their corresponding information and the scheduler determines the best schedule for the dances.

## Built With

**Spark Java** - Lightweight java web framework

**Freemarker** - HTML templating language

**Maven** - Dependencies

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details